﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userProfileController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';
       
            $$('#divInfiniteUserProfile').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#UserProfileSwiper');
                        app.pullToRefreshDone();
                    }
                });

            });
        });

        app.onPageReinit('userProfile', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#UserProfileSwiper');
        });
        app.onPageBeforeAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#UserProfileSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;

            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';
            

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            $scope.username = typeof userLoggedIn != '' && userLoggedIn != null ? userLoggedIn.userName : false;
            $scope.email = userLoggedIn.email;

            $scope.IsUserNameExists = typeof userLoggedIn != 'undefined' && userLoggedIn.userName != null && userLoggedIn.userName != '' && userLoggedIn.userName != ' ' ? true : false;
            $scope.IsEmailExists = typeof userLoggedIn != 'undefined' && userLoggedIn.email != null && userLoggedIn.email != '' && userLoggedIn.email != ' ' ? true : false;

            if (typeof userLoggedIn.photoUrl != 'undefined' && userLoggedIn.photoUrl != null && userLoggedIn.photoUrl != '' && userLoggedIn.photoUrl != ' ') {
                $scope.photoUrl = userLoggedIn.photoUrl;
            }
            else {
                $scope.photoUrl = 'img/logo.png';
            }

            setTimeout(function () {
                $scope.$digest();
            }, fw7.DelayBeforeScopeApply);
        });

        $scope.GoToEditProfile = function () {
            helpers.GoToPage('userDetails', null);
        };

        $scope.GoToUserPlace = function () {
            helpers.GoToPage('userPlace', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
         
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

