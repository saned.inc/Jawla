﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('activationController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';

        });

        app.onPageBeforeAnimation('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';


        });

        app.onPageReinit('activation', function (page) {
            resetForm();
        });

        app.onPageAfterAnimation('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';
            resetForm();
        });

        var resetForm = function () {
            $scope.activationForm.code = null;
            if (typeof $scope.ActivationForm != 'undefined' && $scope.ActivationForm != null) {
                $scope.ActivationForm.$setPristine(true);
                $scope.ActivationForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.activationForm = {};

        $scope.submitForm = function (isValid) {
            if (isValid) {
                var code = $scope.activationForm.code;
                var userId = CookieService.getCookie('UserID');
                var userName = CookieService.getCookie('USName');
                var userPassword = CookieService.getCookie('USPassword');

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activation', "POST", "api/Account/ConfirmEmail", { "userId": userId, "code": code }, function (res) {
                    
                    if (res != null) {
                        CookieService.setCookie('UserEntersCode', true);
                        RegisterDevice(function (result) {
                            appServices.GetToken('login', "POST", "token", userName, userPassword, function (res) {
                                if (res != null) {
                                    CookieService.setCookie('appToken', res.access_token);
                                    CookieService.setCookie('USName', res.userName);
                                    CookieService.setCookie('refreshToken', res.refresh_token);
                                    CookieService.setCookie('Visitor', false);
                                    CookieService.setCookie('loginUsingSocial', false);

                                    appServices.CallService('login', "POST", "api/Account/GetUserInfo", '', function (res) {
                                        SpinnerPlugin.activityStop();

                                        if (res != null) {
                                            if (res.isAmbassador) {
                                                $rootScope.isAmbassador = true;
                                            }
                                            initUserBlocked = false;
                                            CookieService.setCookie('userLoggedIn', JSON.stringify(res.user));
                                            SidePanelService.DrawMenu();
                                            if (typeof res.user.photoUrl != 'undefined' && res.user.photoUrl != null && res.user.photoUrl != '' && res.user.photoUrl != ' ') {
                                                CookieService.setCookie('usrPhoto', res.user.photoUrl);
                                            }
                                            else {
                                                CookieService.removeCookie('usrPhoto');
                                            }

                                            helpers.GoToPage('home', null);
                                        }
                                    });
                                }
                                else {
                                    SpinnerPlugin.activityStop();
                                }
                            });
                        });

                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
                
            }
        };

        $scope.ResendConfirmationCode = function () {
            if (CookieService.getCookie('UserID')) {
                var userId = CookieService.getCookie('UserID');
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activation', "POST", "api/Account/ReSendConfirmationCode/" + userId, '', function (res) {
                    if (res != null) {
                        language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود بنجاح .', 'alert', function () { SpinnerPlugin.activityStop(); });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
            else if (CookieService.getCookie('StoredUserIdForActivation')) {
                var userId = CookieService.getCookie('StoredUserIdForActivation');
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activation', "POST", "api/Account/ReSendConfirmationCode/" + userId, '', function (res) {
                    if (res != null) {
                        language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود بنجاح .', 'alert', function () { SpinnerPlugin.activityStop(); });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
            else {

            }
            
        };

        function RegisterDevice(callBack) {
            helpers.RegisterDevice(function (result) {
                callBack(true);
            });
        }

        $scope.PassToHome = function () {

            RegisterDevice(function (result) {
                CookieService.setCookie('Visitor', true);
                CookieService.setCookie('UserEntersCode', true);
                SidePanelService.DrawMenu();
                helpers.GoToPage('home', null);
            });
        };

        app.init();
    });

}]);

