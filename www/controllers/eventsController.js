﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('eventsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', 'SidePanelService', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var initEvents = true;
    var allEvents = [];
    var mySwiper;
    var categoryId;
    var subCategoryId;
    var subCategoryName;
    var selectedCategoryId

    function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getMonth() + 1), pad(d.getDate()), d.getFullYear()].join('-');
    }

    function ClearListData() {
        allEvents = [];
        loading = false;
        CookieService.setCookie('events-page-number', 1);
        $scope.events = null;
        $scope.eventAlert = false;
    }

    function updateEvents() {
        categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
        subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';

        var params = {
            'categoryId': categoryId,
            'subCategoryId': subCategoryId,
            'PageNumber': 1,
            'PageSize': $rootScope.pageSize,
            'CityId': $rootScope.cityId.id
        };

        $$('.placesFilter').hide();

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
            $scope.events = null;
            ClearListData();
            SpinnerPlugin.activityStop();

            if (response && response.length > 0) {
                $$('.placesFilter').show();
                app.attachInfiniteScroll('#divInfiniteEvents');
                $scope.eventsInfiniteLoader = response.length > 2 ? true : false;
                $scope.events = allEvents;
                $scope.eventAlert = false;

                angular.forEach(response, function (event) {
                    event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                    allEvents.push(event);
                });
            }
            else {
                $$('.placesFilter').hide();
                $scope.events = null;
                $scope.eventAlert = true;
                $scope.eventsInfiniteLoader = false;
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

    }

    InitService.addEventListener('ready', function () {
        $$('.placesFilter').hide();

        app.onPageInit('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';

            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            $scope.categoryId = categoryId;
            $scope.subCategoryId = subCategoryId;
            $scope.txtEventFilter = { value: null };

            $$('#divInfiniteEvents').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.eventsInfiniteLoader = true;

                categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
                subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';

                var params = {
                    'categoryId': categoryId,
                    'subCategoryId': subCategoryId,
                    'PageNumber': parseInt(CookieService.getCookie('events-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    SpinnerPlugin.activityStop();
                    if (response && response.length > 0) {
                        loading = false;

                        CookieService.setCookie('events-page-number', parseInt(CookieService.getCookie('events-page-number')) + 1);
                        angular.forEach(response, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                            allEvents.push(event);
                        });
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                        if (response && response.length < $rootScope.pageSize) {
                            $scope.eventsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteEvents');
                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                            return;
                        }
                    }
                    else {
                        $scope.eventsInfiniteLoader = false;
                    }
                });
            });

            $$('#divInfiniteEvents').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#Eventswiper');
                    }
                });

                categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
                subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';

                var params = {
                    'categoryId': categoryId,
                    'subCategoryId': subCategoryId,
                    'PageNumber': 1,
                    'PageSize': $rootScope.pageSize
                };

                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    $scope.events = null;
                    ClearListData();
                    if (response && response.length > 0) {
                        $scope.eventAlert = false;
                        $scope.events = response;
                        angular.forEach($scope.events, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                        });

                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);

                        app.pullToRefreshDone();
                    }
                    else {
                        $scope.eventAlert = true;
                        app.pullToRefreshDone();
                    }
                });
            });
        });

        app.onPageReinit('events', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#Eventswiper');

            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            subCategoryName = page.query.subCategoryName;
            $scope.categoryId = categoryId;
            $scope.subCategoryId = subCategoryId;
            $scope.txtEventFilter = { value: null };
        });

        app.onPageBeforeAnimation('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';

            $scope.txPlacesFilter = "";
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;

            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#Eventswiper');

            $scope.selectedItem = $rootScope.CurrentuserCity;
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');                  
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';

            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            subCategoryName = page.query.subCategoryName;
            $scope.categoryId = categoryId;
            $scope.subCategoryId = subCategoryId;
            $scope.eventAlert = false;
            $scope.eventsInfiniteLoader = true;
            $scope.txtEventFilter = { value: null };
            updateEvents();
        });
        

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.FilterEvents = function () {
            if (typeof $scope.txtEventFilter.value != 'undefined') {
                ClearListData()

                var params = {};

                var selectedcategoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? $scope.categoryId : '';
                subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';

                if ($scope.txtEventFilter.value == null || $scope.txtEventFilter.value == '' || $scope.txtEventFilter.value == ' ') {
                    params = {
                        CategoryId: selectedcategoryId,
                        subCategoryId: subCategoryId,
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize
                    };
                }
                else {
                    var selectedDate = convertDate($scope.txtEventFilter.value);

                    params = {
                        CategoryId: selectedcategoryId,
                        subCategoryId: subCategoryId,
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize,
                        EventStatyDate: selectedDate,
                       
                    };
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    SpinnerPlugin.activityStop();
                    $scope.events = null;
                    $scope.eventAlert = false;

                    if (response && response.length > 0) {
                        loading = false;
                        $scope.events = response;
                        $scope.eventsInfiniteLoader = response.length > 2 ? true : false;
                        angular.forEach($scope.events, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                        });
                    }
                    else {
                        $scope.eventsInfiniteLoader = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            }
        };

        $scope.filterItems = function (filterText) {
            setTimeout(function () {
                $scope.eventAlert = $scope.events.length > 0 ? false : true;

                if (filterText == '' || filterText == ' ') {
                    $scope.events = allEvents;
                }
                else {
                    $scope.events = allEvents.filter(function (item) {
                        return item.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    });
                }

                $scope.$apply();
            }, 100);
        };

        $scope.goToEventDetails = function (eventId) {
            app.pullToRefreshDone();
            helpers.GoToPage('activitiesDetails', { eventId: eventId, categoryId: $scope.categoryId });
        };

        $scope.GoToPreviousPage = function () {
            app.pullToRefreshDone();
            helpers.GoBack();
        };

        $scope.GoToSearch = function () {
            app.pullToRefreshDone();
            helpers.GoToPage('search', {});
        };

        $scope.goToSearchResult = function (selectedCity) {
            app.pullToRefreshDone();
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            updateEvents();
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

