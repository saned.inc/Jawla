﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('projectsDetailsController', ['$scope', '$rootScope', '$http', '$filter', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, $filter, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var projectId;
    var projectMarkers;
    var myPhotoBrowserPopupDark;
    var ProjectDefautImage;

    InitService.addEventListener('ready', function () {
        app.onPageInit('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            $rootScope.currentOpeningPage = 'projectsDetails';
            $$('#divInfiniteProjectDetails').on('ptr:refresh', function (e) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#ProjectDetailsSwiper');
                        app.pullToRefreshDone();
                    }
                });
            

            });
        });

        app.onPageReinit('projectsDetails', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ProjectDetailsSwiper');
        });

        app.onPageBeforeAnimation('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;


            $rootScope.currentOpeningPage = 'projectsDetails';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ProjectDetailsSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;

        });

        app.onPageAfterAnimation('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            $rootScope.currentOpeningPage = 'projectsDetails';

            $$('#projectDetailsMainMap').show();
             projectId = page.query.projectId;
            var projectParamter = {
                PageNumber: "1",
                PageSize: $rootScope.pageSize,
                Id: projectId
            }

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('projectsDetails', 'POST', "api/CategoryDetails/ViewProject", projectParamter, function (projectDetails) {
                SpinnerPlugin.activityStop();
                if (projectDetails) {
                    projectDetails.municipality = typeof projectDetails.municipality != 'undefined' && projectDetails.municipality != null && projectDetails.municipality != '' && projectDetails.municipality != ' ' ? ' - ' + projectDetails.municipality : '';
                    $scope.project = projectDetails;
                    $scope.idea = projectDetails.idea != null && projectDetails.idea != "" ? true : false;
                    $scope.projectContainImages = typeof projectDetails.images != 'undefined' && projectDetails.images.length > 0 ? true : false;
                    $scope.projectName = projectDetails.name != null && projectDetails.name != "" ? true : false;
                    $scope.projectOwner = projectDetails.owner != null && projectDetails.owner != "" ? true : false;
                    $scope.projectContacts = !projectDetails.projectPhoneNumber && !projectDetails.projectGoogle && !projectDetails.projectWhatsapp && !projectDetails.projectFacebook && !projectDetails.projectTwitter ? true : false;
                    $scope.projectOwnerContacts = !projectDetails.ownerPhoneNumber && !projectDetails.ownerGoogle && !projectDetails.ownerWhatsapp && !projectDetails.ownerFacebook && !projectDetails.ownerTwitter ? true : false;
                    ProjectDefautImage = $filter("filter")(projectDetails.images, { isDefault: true })[0];
                    $scope.projectImage = typeof ProjectDefautImage != 'undefined' && ProjectDefautImage != null ? ProjectDefautImage.imageUrl : 'img/pic.jpg';

                    document.addEventListener("deviceready", function () {
                                    navigator.geolocation.getCurrentPosition(function (position) {

                                        $rootScope.firstAddress = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                        $rootScope.secondAddress = new google.maps.LatLng(projectDetails.latitude, projectDetails.longitude);
                                        $scope.currentLatitude = position.coords.latitude;
                                        $scope.currentLongitude = position.coords.longitude;
                                        $scope.DestinationLatitude = projectDetails.latitude;
                                        $scope.DestinationLongitude = projectDetails.longitude;

                                      var  projectMarkers = [{ "title": projectDetails.name, "lat": projectDetails.latitude, "lng": projectDetails.longitude, "description": projectDetails.description }];
                                      helpers.initMap('mapProject', projectMarkers, 'projectsDetails', '', function (map) {
                                          $$('#projectDetailsMainMap').hide();
                                      });

                                    }, function (PositionError) {
                                        var error = PositionError;
                                    }, { enableHighAccuracy: true });
                        // cordova.plugins.locationAccuracy.canRequest(function (canRequest) {
                        //     if (canRequest) {
                        //         cordova.plugins.locationAccuracy.request(function (success) {
                        //         }, function (error) {
                        //             language.openFrameworkModal('خطأ', 'من فضلك فم بفتح نظام تحديد المواقع (gps)  .', 'alert', function () { });
                        //         }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                        //     }
                        // });

                    }, false);

                    if (typeof projectDetails.images != 'undefined' && projectDetails.images.length > 0) {
                        var photosArray = [];
                        angular.forEach(projectDetails.images, function (photo) {
                            photo.imageUrl = photo.imageUrl ? photo.imageUrl : 'img/pic.jpg';
                            var photoObj = { url: hostUrl + 'Uploads/' + photo.imageUrl };                        
                            photosArray.push(photoObj);
                        });

                        if (photosArray.length > 0) {
                            myPhotoBrowserPopupDark = app.photoBrowser({
                                photos: photosArray,
                                theme: 'dark',
                                backLinkText: 'إغلاق',
                                lazyLoading:true,
                                ofText: 'من',
                                type: 'popup'
                            });
                        }
                    }
      
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });

        });

        $scope.openEventGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.matchSocial = function (socialLink) {
            if ((socialLink).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/))
                cordova.InAppBrowser.open(socialLink, '_system', 'location=no');
            else
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
        }

        $scope.projectOwnerContact = function (contactType, object) {
            if (contactType == "call") {
                window.plugins.CallNumber.callNumber(
               function onSuccess(successResult) {
                   console.log("Success:" + successResult);
               }, function onError(errorResult) {
                   console.log("Error:" + errorResult);
               }, object, true);
            }
            else if (contactType == "whatsapp") {
                window.open('whatsapp://send?= مرحبا اخى&phone=+966' + object, "_system");
            }
            else if (contactType == "facebok") {
                $scope.matchSocial(object);
            }
            else if (contactType == "twitter") {
                $scope.matchSocial(object);
            }
            else if (contactType == "googleplus") {
                $scope.matchSocial(object);
            }
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

          $scope.goToSearchResult = function (selectedCity) {
              $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
              $('#linkHomeBannerCity .item-after').html('');
              $rootScope.CurrentuserCity = selectedCity;
              $rootScope.cityName = selectedCity.name;
              $rootScope.cityId = selectedCity;
           
          };

          $scope.openProjectMap = function () {
              launchnavigator.navigate([$scope.DestinationLatitude, $scope.DestinationLongitude], {
                  start: [$scope.currentLatitude, $scope.currentLongitude]
              });
          }

          $scope.GoToUber = function (project) {
              language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
          };

          $scope.GoToCareem = function (project) {
              language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
          };

          $scope.GoToCaptain = function (place) {
              language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
          };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

