﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('ambassadorDetailsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var ambassadorId;

    InitService.addEventListener('ready', function () {
        app.onPageInit('ambassadorDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorDetails') return;
            $rootScope.currentOpeningPage = 'ambassadorDetails';           
            $scope.NoselectionOfCityYet = true;
            $$('#divInfiniteAmbassadorDetailsPullToRefresh').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#ambassadorDetailswiper');
                        app.pullToRefreshDone();
                    }
                });

            });
        });

        app.onPageReinit('ambassadorDetails', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ambassadorDetailswiper');
        });
        app.onPageBeforeAnimation('ambassadorDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorDetails') return;
            $rootScope.currentOpeningPage = 'ambassadorDetails';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ambassadorDetailswiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('ambassadorDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorDetails') return;
            $rootScope.currentOpeningPage = 'ambassadorDetails';
            
            $scope.PlacesOfCoverage = false;
            ambassadorId = page.query.ambassadorId;
            //$scope.selectedItem = page.query.currentCity;
            //$scope.CurrentuserCityName = page.query.cityName;

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('ambassadorProfile', 'POST', "Api/Ambassador/GetAmbassador", {
                "AmbassadorId": ambassadorId,
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            }, function (ambassadorProfileResult) {
                SpinnerPlugin.activityStop();
                if (ambassadorProfileResult) {
                    if (ambassadorProfileResult.items.length == 0 || ambassadorProfileResult.items == null) {
                        $scope.PlacesOfCoverage = true;
                    }

                    if (typeof ambassadorProfileResult.photoUrl != 'undefined' && ambassadorProfileResult.photoUrl != null && ambassadorProfileResult.photoUrl != '' && ambassadorProfileResult.photoUrl != ' ') {
                        ambassadorProfileResult.photoUrl = ambassadorProfileResult.photoUrl;
                    }
                    else {
                        ambassadorProfileResult.photoUrl = 'img/logo.png';
                    }

                    $scope.ambassador = ambassadorProfileResult;
                    $scope.ambassadorImage = ambassadorProfileResult.photoUrl ? ambassadorProfileResult.photoUrl : "img/logo.png";
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {           
                    language.openFrameworkModal('خطأ', 'خطا في استرجاع بيانات السفير', 'alert', function () { });
                    helpers.GoToPage('placeDetails', null);
                }
            });
        });

        $scope.callWithAmbassador = function (ambassadorPhone) {
            var bypassAppChooser = true;
            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {
                  console.log("Success:" + successResult);
              }, function onError(errorResult) {
                  console.log("Error:" + errorResult);
              }, ambassadorPhone, bypassAppChooser);
        }

        $scope.emailWithAmbassador = function (ambassadorEmail) {
            cordova.InAppBrowser.open("mailto:" + ambassadorEmail + '?subject=' + 'جولة' + '&body=' + 'محتوي الرسالة', '_system', 'location=no,toolbar=no,zoom=no');

        }

        $scope.goToCoveredPageDetails = function (coveredPageId, categoryId) {
            app.pullToRefreshDone();
            if (categoryId < 5) {
                helpers.GoToPage('placeDetails', { placeId: coveredPageId });
            }
            else if (categoryId == 5) {
                helpers.GoToPage('activitiesDetails', { eventId: coveredPageId });
            }
            else if (categoryId == 6) {
                helpers.GoToPage('projectsDetails', { projectId: coveredPageId });
            }         
        }

        $scope.GoToSearch = function () {
            app.pullToRefreshDone();
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            app.pullToRefreshDone();
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            app.pullToRefreshDone();
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
         
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });
}]);

