﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('visitorsOpinionController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var placeId;
    var visitorCoveredId;
    var visitorOpinionPhotosArray = [];
    var isCommentOwner = false;
    var loading;
    var placeDetails;
    var placeRate;
    var userRating;
    var placeName;
    var allComments = [];
    $scope.visitorOpinionCommentForm = {};
    var visitorOpinionArray = [];
    var myPhotoBrowserPopupDark;

    InitService.addEventListener('ready', function () {

        app.onPageInit('visitorsOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'visitorsOpinion') return;
            $rootScope.currentOpeningPage = 'visitorsOpinion';

            $$('#divInfiniteVisitorOpinion').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#VisitorOpinionwiper');
                    }
                });
                $scope.comments = null;
                allComments = [];
                $scope.resetPhotos();
                $scope.getAllVistorsImages();
                getCommentsForVisitorOpinion();
            });

            $$('#divInfiniteVisitorOpinion').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.visitorOpinionMoreThanThreeComments = true;

                var pageNumber = parseInt(CookieService.getCookie('vistorOpinion-comments-page-number')) + 1;
                var pageSize = $rootScope.pageSize;;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('visitorsOpinion', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + placeId + "/1", '', function (comments) {
                    SpinnerPlugin.activityStop();
                    if (comments && comments.length > 0) {
                        loading = false;
                        CookieService.setCookie('vistorOpinion-comments-page-number', parseInt(CookieService.getCookie('visitorOpinion-comments-page-number')) + 1);

                        angular.forEach(comments, function (comment) {
                            isCommentOwner = false;
                            if (CookieService.getCookie('userLoggedIn')) {
                                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                                comment.fullName = comment.fullName ? comment.fullName : comment.userName;
                                isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                                comment.photoUrl = comment.photoUrl ? comment.photoUrl : 'img/profile.png';
                            }
                            comment.isCommentOwner = isCommentOwner;
                            allComments.push(comment);
                        });
                        $scope.visitorOpinionAlert = false;
                        $scope.comments = allComments;
                        $scope.visitorOpinionContainsComments = true;                        
                        if (comments && comments.length < $rootScope.pageSize) {
                            $scope.visitorOpinionMoreThanThreeComments = false;
                            app.detachInfiniteScroll('#divInfiniteVisitorOpinion');
                            return;
                        }

                    }
                    else {
                        $scope.visitorOpinionMoreThanThreeComments = false;
                        app.detachInfiniteScroll('#divInfiniteVisitorOpinion');
                        return;
                    }
                });
            });


        });

        app.onPageBeforeAnimation('visitorsOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'visitorsOpinion') return;
            $rootScope.currentOpeningPage = 'visitorsOpinion';

            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = null;
            $scope.Advertisements = advertisments;

            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#VisitorOpinionwiper');

            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            CookieService.setCookie('vistorOpinion-comments-page-number', 1);
            $scope.cites = myApp.fw7.Cities;
            $scope.userLogin = false;
            $scope.isUserVisitor = true;
            if (CookieService.getCookie('userLoggedIn')) {
                $scope.userLogin = true;
                $scope.isUserVisitor = false;
            }
        });

        app.onPageAfterAnimation('visitorsOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'visitorsOpinion') return;
            $rootScope.currentOpeningPage = 'visitorsOpinion';

            if (page.fromPage.name != 'replies') {
                placeDetails = page.query.placeDetails;
                placeId = page.query.id;

                placeDetails.rating = parseInt(placeDetails.rating / 2);
                placeDetails.userRating = parseInt(placeDetails.userRating / 2);

                $scope.place = placeDetails;
                $scope.visitorOpinionContainsImages = false;
                $scope.visitorOpinionAlert = false;
                getCommentsForVisitorOpinion();
                $scope.resetPhotos();
                resetCommentForm();
                $scope.resetComments();
                
                $scope.getAllVistorsImages();
            }
            else {
                getCommentsForVisitorOpinion();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });


        app.onPageReinit('visitorsOpinion', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#VisitorOpinionwiper');

            getCommentsForVisitorOpinion();
        });

        $scope.getAllVistorsImages = function () {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('visitorsOpinion', 'POST', "api/VisitorCover/ViewPhotos", { "CategoryDetailsId": placeId }, function (photoResult) {
                SpinnerPlugin.activityStop();
                if (photoResult && photoResult.length > 0) {
                    $scope.visitorOpinionContainsImages = true;
                   
                    if (photoResult.length >= 4) {
                        for (var i = 1; i <= photoResult.length; i++) {
                            var photoObj = { url: hostUrl + 'Uploads/' + photoResult[photoResult.length - i].photoUrl, caption: photoResult[photoResult.length - i].title };
                            visitorOpinionPhotosArray.push(photoObj);
                        }
                    }
                    else {
                        for (var i = 0; i < photoResult.length; i++) {
                            var photoObj = { url: hostUrl + 'Uploads/' + photoResult[i].photoUrl, caption: photoResult[i].title };
                            visitorOpinionPhotosArray.push(photoObj);
                        }
                    }

                    if (visitorOpinionPhotosArray.length > 0) {
                        myPhotoBrowserPopupDark = app.photoBrowser({
                            photos: visitorOpinionPhotosArray,
                            theme: 'dark',
                            backLinkText: 'إغلاق',
                            lazyLoading: true,
                            maxZoom: 4,
                            ofText: 'من',
                            type: 'popup'
                        });
                    }
                   
                    if (photoResult.length >= 4) {
                        for (var i = 1; i < 5 ; i++) {
                            visitorOpinionArray.push(photoResult[photoResult.length - i]);
                        }
                        $scope.photos = visitorOpinionArray;
                        app.pullToRefreshDone();
                    }
                    else {
                        $scope.photos = photoResult;
                        app.pullToRefreshDone();
                    }

                }
                else {
                    app.pullToRefreshDone();
                    $scope.visitorOpinionContainsImages = false;
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
            });

        }

        $scope.openVisitorOpinionGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.DeleteComment = function (comment) {
            language.openFrameworkModal('تأكيد', 'هل انت متأكد من حذف التعليق ؟', 'confrim', function () {

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('visitorsOpinion', 'POST', "api/Comments/DeleteComment/" + comment.id, '', function (result) {
                    SpinnerPlugin.activityStop();
                    if (result != null) {
                        $scope.comments = $scope.comments.filter(function (item) {
                            return item.id !== comment.id;
                        });

                        $scope.visitorOpinionAlert = $scope.comments != null && $scope.comments.length > 0 ? false : true;
                        $scope.visitorOpinionContainsComments = $scope.comments != null && $scope.comments.length > 0 ? true : false;
                        $scope.visitorOpinionMoreThanThreeComments = $scope.comments != null && $scope.comments.length > 3 ? true : false;
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {

                       
                    }
                });
            });
        };

       var getCommentsForVisitorOpinion = function () {
            var pageNumber = 1;
            var pageSize = $rootScope.pageSize;
            allComments = [];
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('visitorsOpinion', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + placeId + "/1", '', function (comments) {
                SpinnerPlugin.activityStop();
                if (comments && comments.length > 0) {
                    app.attachInfiniteScroll('#infiniteLoaderVisitorOpinion');
                    $scope.visitorOpinionMoreThanThreeComments = false;

                    angular.forEach(comments, function (comment) {
                        if (CookieService.getCookie('userLoggedIn')) {
                            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                            comment.fullName = comment.fullName ? comment.fullName : comment.userName;
                            isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                            comment.photoUrl = comment.photoUrl ? comment.photoUrl : 'img/profile.png';
                        }
                        comment.isCommentOwner = isCommentOwner;
                        allComments.push(comment);
                    });

                    $scope.visitorOpinionContainsComments = true;
                    $scope.visitorOpinionAlert = false;
                
                    $scope.comments = allComments;
                }
                else {
                    $scope.comments = [];
                    $scope.visitorOpinionAlert = true;
                    $scope.visitorOpinionMoreThanThreeComments = false;
                    $scope.visitorOpinionContainsComments = false;
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }

       $scope.goToUserOpinion = function () {
           if (CookieService.getCookie('userLoggedIn')) {
               var params = {
                   Id: placeId
               };

               SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
               appServices.CallService('userOpinion', "POST", "api/VisitorCover/GetCoverDetails", params, function (res) {
                   SpinnerPlugin.activityStop();
                   if (res != null) {
                       helpers.GoToPage('userOpinion', {
                           visitorCoverId: placeId
                       });
                   }
                   else {
                       language.openFrameworkModal('تنبيه', 'قم بالتقييم او اضافة كومنت اولا', 'alert', function () { });
                   }
               });
           }
           else {
               language.openFrameworkModal('تنبيه', 'قم بتسجيل الدخول اولا ', 'alert', function () { });
           }

       
            
        }

        $scope.submitForm = function (isValid) {
            $scope.visitorOpinionCommentReset = true;
            if (isValid) {
                if (CookieService.getCookie('userLoggedIn')) {
                    var params = {
                        'CategoryDetailsId': placeId
                    };

                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    appServices.CallService('visitorsOpinion', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (res) {
                        if (res != null) {
                            var params = {
                                CommentText: $scope.visitorOpinionCommentForm.commentText,
                                CommentTypeId: 1,
                                ParentId: null,
                                RelatedId: placeId
                            };

                          
                            appServices.CallService('userOpinion', 'POST', "api/Comments/SaveComment", params, function (result) {
                                SpinnerPlugin.activityStop();
                            if (result) {
                               resetCommentForm();                         
                                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                    result.fullName = result.fullName ? result.fullName : result.userName;
                                    if (typeof userLoggedIn.photoUrl != 'undefined' && userLoggedIn.photoUrl != null && userLoggedIn.photoUrl != '' && userLoggedIn.photoUrl != ' ') {
                                        result.photoUrl = userLoggedIn.photoUrl;
                                    }
                                    else {
                                        result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';
                                    }
                                    
                                    isCommentOwner = userLoggedIn.id == result.userId ? true : false;
                                    result.isCommentOwner = isCommentOwner;

                                $scope.comments.splice(0, 0, result);
                                $scope.visitorOpinionAlert = false;
                                $scope.visitorOpinionContainsComments = true;
                                $scope.visitorOpinionMoreThanThreeComments =  false;
                                setTimeout(function () {
                                    $scope.$apply();
                                }, fw7.DelayBeforeScopeApply);
                            }
                            else {
                                SpinnerPlugin.activityStop();
                                setTimeout(function () {
                                    $scope.$apply();
                                }, fw7.DelayBeforeScopeApply);
                            }
                        });

                        }
                    });
                }
                else {
                    language.openFrameworkModal('تنبيه', 'قم بتسجيل الدخول اولا', 'alert', function () { });
                }

            }
        };

        $scope.resetPhotos = function () {        
            visitorOpinionPhotosArray = [];
            visitorOpinionArray = [];
            $scope.photos = [];
        }

        $scope.resetComments = function () {
            allComments = [];
            $scope.comments = [];       
        }

        var resetCommentForm = function () {
            $scope.visitorOpinionCommentReset = false;
            $scope.visitorOpinionCommentForm.commentText = null;
            if (typeof $scope.VisitorOpinionCommentForm != 'undefined' && $scope.VisitorOpinionCommentForm != null) {
                $scope.VisitorOpinionCommentForm.$setPristine(true);
                $scope.VisitorOpinionCommentForm.$setUntouched();
            }
        };

        $scope.onPlaceRating = function (rate) {
            var params = {
                'CategoryDetailsId': placeId
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('visitorsOpinion', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (res) {
                if (res!=null) {
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    if (userLoggedIn) {
                        var ratingParams = {
                            "RateValues": "1:" + parseInt(rate * 2),
                            "RelatedId": placeId,
                            "UserId": userLoggedIn.id,
                            "RelatedType": "1",
                            "Description": parseInt(rate * 2)
                        };
                        appServices.CallService('visitorsOpinion', 'POST', "api/Rating/SaveRating", ratingParams, function (ratingResult) {
                            SpinnerPlugin.activityStop();
                            if (ratingResult == 1) {
                                language.openFrameworkModal('تم', 'تم اضافة التقييم بنجاح', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('تنبيه', 'خطا اثناء اضافة التقييم', 'alert', function () { });
                            }
                        });
                    }
                    else {
                        language.openFrameworkModal('تنبيه', 'قم بتسجيل الدخول اولا', 'alert', function () { });

                    }
                }

            });
     
        }

        $scope.OpenReplies = function (comment) {
            CookieService.setCookie('requiredComment', JSON.stringify(comment));
            CookieService.setCookie('requiredEvent', JSON.stringify(placeDetails));
            helpers.GoToPage('replies', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {
            });
        };

        $scope.GoToPreviousPage = function () {
        
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
         
        };
      
        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };
        app.init();
    });

}]);

