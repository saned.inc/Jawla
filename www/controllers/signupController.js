﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('signup', function (page) {
            if ($rootScope.currentOpeningPage != 'signup') return;
            $rootScope.currentOpeningPage = 'signup';
        });

        app.onPageBeforeAnimation('signup', function (page) {
            if ($rootScope.currentOpeningPage != 'signup') return;
            $rootScope.currentOpeningPage = 'signup';
            resetSignUpForm();

        });

        app.onPageAfterAnimation('signup', function (page) {
            if ($rootScope.currentOpeningPage != 'signup') return;
            $rootScope.currentOpeningPage = 'signup';          
        });
       
        var resetSignUpForm = function () {
            $scope.signUpReset = false;
            $scope.userForm.username = null;
            $scope.userForm.email = null;
            $scope.userForm.password = null;
            $scope.userForm.confirmpassword = null;
            if (typeof $scope.SignupForm != 'undefined' && $scope.SignupForm != null) {
                $scope.SignupForm.$setPristine(true);
                $scope.SignupForm.$setUntouched();
            }
        }
       
        $scope.submitForm = function (isValid) {
            $scope.signUpReset = true;
            if (isValid) {               
                var user = {
                    'userName': $scope.userForm.username,
                    'Email': $scope.userForm.email,
                    'Password': $scope.userForm.password,
                    'ConfirmPassword': $scope.userForm.confirmpassword,
                    'Role': 'User'
                }

                CookieService.setCookie('USName', $scope.userForm.username);
                CookieService.setCookie('USPassword', $scope.userForm.password);

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('signup', "POST", "api/Account/Register", user, function (res2) {
                    if (res2 != null) {
                        SpinnerPlugin.activityStop();
                        CookieService.setCookie('UserID', res2);
                        CookieService.setCookie('StoredUserIdForActivation', res2);
                        CookieService.setCookie('UserEntersCode', false);
                        language.openFrameworkModal('نجاح', 'تم تسجيل المستخدم بنجاح .', 'alert', function () {
                            helpers.GoToPage('activation', null);
                        });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
        };

        $scope.form = {};
        $scope.userForm = {};

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();

    });
}]);

