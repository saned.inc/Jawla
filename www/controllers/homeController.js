﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('homeController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
  
    $rootScope.MatchCityId = function () {
        $scope.NoselectionOfCityYet = true;
        for (var i = 0; i < $scope.cites.length; i++) {
            if ($scope.cites[i].name.indexOf($rootScope.currentLocation) !== -1) {
                $rootScope.cityId = $scope.cites[i];
                return $scope.cites[i];
            }
        }
        $rootScope.cityId = $scope.cites[0];
        return $scope.cites[0];
        
    }

    function LoadCountriesAndCities(callBack) {
        if (myApp.fw7.Countries == null || myApp.fw7.Countries.length == 0) {
            appServices.CallService('home', 'GET', "api/Country/GetCountries", "", function (countries) {
                if (countries) {
                    $rootScope.soadiaId = countries[0].id;

                    $scope.countries = countries;
                    myApp.fw7.Countries = countries;
                    if (countries.length > 0) {
                        
                        appServices.CallService('home', 'GET', "api/City/FindCities/" + countries[0].id, "", function (cities) {
                            if (cities) {                                                    
                                $scope.cites = cities;
                                $rootScope.CurrentuserCity = $rootScope.MatchCityId();
                                $rootScope.CurrentuserCityId = $rootScope.CurrentuserCity.id;
                                $scope.selectedItem = $rootScope.CurrentuserCity;
                                $scope.CurrentuserCityName = $rootScope.CurrentuserCity.name;
                                myApp.fw7.Cities = cities;
                                callBack(true);
                            }
                        });
                    }
                }
            });
        }
        else {
            $scope.cites = myApp.fw7.Cities;
            $rootScope.CurrentuserCity = $rootScope.MatchCityId();
            $rootScope.CurrentuserCityId = $rootScope.CurrentuserCity.id;
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.CurrentuserCityName = $rootScope.CurrentuserCity.name;
            callBack(true);
        }
    }

    function LoadCategories(callBack) {
        if (myApp.fw7.Categories == null || myApp.fw7.Categories.length == 0) {
            appServices.CallService('home', 'POST', "api/Category/GetCategories", { PageNumber: '0', PageSize: $rootScope.pageSize }, function (categories) {
                if (categories) {
                    $scope.categories = categories;
                    CookieService.setCookie('homeCategories', categories);
                    myApp.fw7.Categories = categories;
                    callBack(true);
                }
            });
        }
        else {
            callBack(true);
        }
    }

   $rootScope.LoadAdvertisements=function(callBack) {
       SidePanelService.loadAdvertisments(function (result) {
            $scope.Advertisements = null;
            $scope.Advertisements = result;
            callBack(result);
        });
    }

    function LoadAllPublicData(callBack) {
        //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        LoadCountriesAndCities(function (IsCountriesLoaded) {
            LoadCategories(function (IsCategoriesLoaded) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    SpinnerPlugin.activityStop();
                    callBack(true);
                });
            });
        });
    }

    function LoadData() {
        SidePanelService.DrawMenu();

        $scope.currentLocation = $rootScope.currentLocation;
        $scope.selectedItem = $rootScope.currentLocation;
        $scope.selectedItem = $rootScope.CurrentuserCity;
        LoadAllPublicData(function (result) {
            if (result != null && result == true) {
                var advertisments = myApp.fw7.Advertisements;
                $scope.Advertisements = advertisments;

                if (advertisments !== undefined && advertisments.length > 0) {
                    $rootScope.initalSwiper('#homeSwiper');
                }
            }

        });
        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $document.ready(function () {
        app.onPageInit('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';
            var name1 = page.query.name1;
            SidePanelService.DrawMenu();
            $$('#divInfiniteHomePullToRefresh').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#homeSwiper');
                        app.pullToRefreshDone();
                    }
                });
               
            });
            LoadData();

        });

        
        app.onPageReinit('home', function (page) {       
            //if ($rootScope.swiper) {
            //    $rootScope.swiper.destroy(true, true);
            //}

            if (myApp.fw7.Countries == null || myApp.fw7.Countries.length == 0) {
                LoadCountriesAndCities(function (IsCountriesLoaded) {
                    LoadCategories(function (IsCategoriesLoaded) {
                        $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                            $scope.cites = myApp.fw7.Cities;
                            $rootScope.CurrentuserCity = $rootScope.MatchCityId();
                            $rootScope.CurrentuserCityId = $rootScope.CurrentuserCity.id;
                            $scope.selectedItem = $rootScope.CurrentuserCity;
                            $scope.CurrentuserCityName = $rootScope.CurrentuserCity.name;
                            $scope.currentLocation = $rootScope.CurrentuserCity.name;
                        });
                    });
                });
            }
            else {
                $scope.cites = myApp.fw7.Cities;
                $rootScope.CurrentuserCity = $rootScope.MatchCityId();
                $rootScope.CurrentuserCityId = $rootScope.CurrentuserCity.id;
                $scope.selectedItem = $rootScope.CurrentuserCity;
                $scope.CurrentuserCityName = $rootScope.CurrentuserCity.name;
                $scope.currentLocation = $rootScope.CurrentuserCity.name;
            }

            //LoadData();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        
        $rootScope.initalSwiper = function (id) {
            var counter = 1;
            var counterSaved;
            var fingerSwiper = false;
            var max = myApp.fw7.Advertisements.length;
            $rootScope.swiper = new Swiper(id, {
                loop: false,
                autoplayDisableOnInteraction: false,        
                spaceBetween: 5,
                center: true,
                speed:2000,
                slidesPerView: 1,
                autoplay: myApp.fw7.Advertisements[0].secondNumber * 1000,
                //onSliderMove: function (swiper, event) {
                //    fingerSwiper = true;
                //    counter = counterSaved;
                //},
                onSlideChangeStart: function (swiper) {
                        var time = parseInt(myApp.fw7.Advertisements[counter].secondNumber * 1000);
                        counter++;
                        swiper.params.autoplay = time;
                        if (counter >= parseInt(max)) {
                            counter = 0;
                            time = myApp.fw7.Advertisements[0].secondNumber * 1000;
                        }
                                    
                }               
            });

            setTimeout(function () {
                $rootScope.swiper.update();
                $scope.$apply();
            }, 500);
        }


        app.onPageBeforeAnimation('home', function (page) {          
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';
            LoadData();
        });

        app.onPageAfterAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';

        });

        $scope.goToCategoryDetails = function (category) {
            if (category.hasChildren) {
                app.pullToRefreshDone();
                helpers.GoToPage('subCategories', { categoryId: category.id });
            }
            else {
                if (category.id < 5) {
                    app.pullToRefreshDone();
                    helpers.GoToPage('places', { categoryId: category.id });
                }
                else if (category.id == 5) {
                    app.pullToRefreshDone();
                    helpers.GoToPage('events', { categoryId: category.id });
                }
                else if (category.id == 6) {
                    app.pullToRefreshDone();
                    helpers.GoToPage('projects', { categoryId: category.id });
                }
            }
        };

        $scope.GoToSearch = function () {
            app.pullToRefreshDone();
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            app.pullToRefreshDone();
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            app.pullToRefreshDone();
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
     
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

