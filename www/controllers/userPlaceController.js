﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userPlaceController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading = false;
    var categoryId;
    var initUserPlacesInfinite;
    var allUserPlaces = [];

    InitService.addEventListener('ready', function () {
        app.onPageInit('userPlace', function (page) {
            if ($rootScope.currentOpeningPage != 'userPlace') return;
            $rootScope.currentOpeningPage = 'userPlace';

            $$('#divInfiniteUserPlaces').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.userPlacesInfiniteLoader = true;
                var params = {
                    'PageNumber': parseInt(CookieService.getCookie('userPlaces-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('userPlace', 'POST', "api/VisitorCover/GetCoverList", params, function (userPlacesResult) {
                    SpinnerPlugin.activityStop();
                    if (userPlacesResult && userPlacesResult.length > 0) {
                        $scope.categoryName = userPlacesResult[0].category;
                        loading = false;
                        CookieService.setCookie('userPlaces-page-number', parseInt(CookieService.getCookie('userPlaces-page-number')) + 1);

                        angular.forEach(userPlacesResult, function (userPlace) {
                            userPlace.rating = parseInt(parseInt(userPlace.rating) / 2);
                            userPlace.imageUrl = userPlace.imageUrl ? userPlace.imageUrl : 'img/pic.jpg';
                            allUserPlaces.push(userPlace);
                        });

                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (userPlacesResult && userPlacesResult.length <= $rootScope.pageSize) {
                            $scope.userPlacesInfiniteLoader = false;
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                            app.detachInfiniteScroll('#divInfiniteUserPlaces');
                            return;
                        }
                    }
                    else {
                        $scope.userPlacesInfiniteLoader = false;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            });

            $$('#divInfiniteUserPlaces').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $scope.updateUserPlaces();
            });
        });

        app.onPageBeforeAnimation('userPlace', function (page) {
            if ($rootScope.currentOpeningPage != 'userPlace') return;
            $rootScope.currentOpeningPage = 'userPlace';

            $scope.cites = myApp.fw7.Cities;

            //setTimeout(function () {
            //    $scope.$digest();
            //}, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userPlace', function (page) {
            if ($rootScope.currentOpeningPage != 'userPlace') return;
            $rootScope.currentOpeningPage = 'userPlace';
            

            $scope.resetUserPlacesScope();
            $scope.userPlacesInfiniteLoader = true;
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            $scope.updateUserPlaces();

            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if (advertisments !== undefined && advertisments.length > 0) {
                var counter = 0;
                var max = advertisments.length;
                var mySwiper = app.swiper('#UserPlaceSwiper', {
                    pagination: '.swiper-pagination',
                    loop: true,
                    lazyLoading: true,
                    preloadImages: false,
                    autoplayDisableOnInteraction: false,
                    onSlideNextStart: function (swiper) {
                        var time = parseInt(advertisments[counter].secondNumber * 1000);
                        setTimeout(function () {
                            counter++;
                            if (counter >= parseInt(max)) {
                                counter = 0;
                            }
                            swiper.slideNext();
                        }, time);
                    }
                });
            }
        });

        $scope.goToUserPlaceCoverage = function (visitorCoverId) {
            helpers.GoToPage('userOpinion', { visitorCoverId: visitorCoverId });
        }

        $scope.updateUserPlaces = function () {
            $scope.resetUserPlacesScope();
            var params = {
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            }
            appServices.CallService('places', 'POST', "api/VisitorCover/GetCoverList", params, function (placesResult) {
                SpinnerPlugin.activityStop();
                if (placesResult && placesResult.length > 0) {
                    $scope.categoryName = placesResult[0].category;
                    $scope.userPlacesInfiniteLoader = false;
                    app.attachInfiniteScroll('#divInfinitePlaces');
                    angular.forEach(placesResult, function (place) {
                        place.rating = parseInt(parseInt(place.rating) / 2);
                        place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                        allUserPlaces.push(place);
                    });
                    $scope.userPlaces = allUserPlaces;
                    app.pullToRefreshDone();
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.userPlaceAlert = true;
                    app.pullToRefreshDone();
                    $scope.userPlacesInfiniteLoader = false;
                    $scope.categoryType = "لا توجد أماكن قد سبق لك تغطيتها سواء بالتعليق أو التقييم .";
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });
        }

        $scope.resetUserPlacesScope = function () {
            CookieService.setCookie('userPlaces-page-number', 1);
            $scope.userPlaceAlert = false;
            $scope.userPlaces = null;
            loading = false;
            allUserPlaces = [];
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

          $scope.goToSearchResult = function (selectedCity) {
            $scope.selectedItem = '';
            $('#linkHomeBannerCity .item-after').html('المدينة');
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedId.id, municipality: "", category: "", place: "", currentPlaceLatitude: "", currentPlaceLongitude: "" });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

