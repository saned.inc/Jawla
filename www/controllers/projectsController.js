﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('projectsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', 'SidePanelService', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var categoryId;
    var subCategoryId;
    var subCategoryName;
    var selectedCategoryId
    var allProjects = [];

    InitService.addEventListener('ready', function () {
        $$('.placesFilter').hide();

        app.onPageInit('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';          
            $$('#divInfiniteProjects').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.projectsInfiniteLoader = true;

                categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
                var params = {
                    'categoryId': categoryId,
                    'subCategoryId': subCategoryId,
                    'PageNumber': parseInt(CookieService.getCookie('projects-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('projects', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (projectsResults) {
                    SpinnerPlugin.activityStop();
                    if (projectsResults && projectsResults.length > 0) {
                        $scope.projectsInfiniteLoader = false;
                        loading = false;
                        CookieService.setCookie('projects-page-number', parseInt(CookieService.getCookie('projects-page-number')) + 1);
                        angular.forEach(projectsResults, function (project) {
                            project.rating = parseInt(project.rating / 2);
                            project.imageUrl = project.imageUrl ? project.imageUrl : 'img/pic.jpg';
                            allProjects.push(project);
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (projectsResults && projectsResults.length < $rootScope.PageSize) {                         
                            $scope.projectsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteProjects');
                            return;
                        }
                    }
                    else {
                        $scope.projectsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteProjects');
                        return;
                    }
                });
            });

            $$('#divInfiniteProjects').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#homeSwiper');
                     }
                });

                updateProjects();
            });
        });

        app.onPageReinit('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';

            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ProjectSwiper');
            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            subCategoryName = page.query.subCategoryName;
        });

        app.onPageBeforeAnimation('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';
            $scope.txPlacesFilter = "";
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#ProjectSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;
            selectedCategoryId = page.query.categoryId;
        });

        app.onPageAfterAnimation('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';

            $scope.projectsInfiniteLoader = true;
            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            subCategoryName = page.query.subCategoryName;
            $scope.resetProjectsScope();
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            updateProjects();
        });

        $scope.gotoProjectDetails = function (projectId) {
            helpers.GoToPage('projectsDetails', { projectId: projectId });
        }
        
        var updateProjects = function () {
            $scope.resetProjectsScope();
            categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
            subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';

            var projectsParamter = {
                'categoryId': categoryId,
                'subCategoryId': subCategoryId,
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize,
                'CityId': $rootScope.cityId.id
            }

            $$('.placesFilter').hide();

            appServices.CallService('projects', 'POST', "api/CategoryDetails/SearchCategoryResult", projectsParamter, function (projectResult) {
                SpinnerPlugin.activityStop();
                if (projectResult && projectResult.length > 0) {
                    $$('.placesFilter').show();
                    $scope.ProjectsAlert = false;
                    $scope.projectsInfiniteLoader = false;
                    angular.forEach(projectResult, function (project) {
                        project.rating = parseInt(project.rating / 2);
                        project.imageUrl = project.imageUrl ? project.imageUrl : 'img/pic.jpg';
                        allProjects.push(project);
                        app.attachInfiniteScroll('#divInfiniteProjects');
                    });
                    app.pullToRefreshDone();
                    $scope.projects = allProjects;
                }
                else {
                    $$('.placesFilter').hide();
                    $scope.projectsInfiniteLoader = false;
                    $scope.ProjectsAlert = true;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });
        }

        $scope.resetProjectsScope = function () {
            CookieService.setCookie('projects-page-number', 1);
            $scope.projects = null;
            allProjects = [];
        }

        $scope.GoToSearch = function () {
            app.pullToRefreshDone();
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            app.pullToRefreshDone();
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            app.pullToRefreshDone();
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            updateProjects();
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        $scope.filterItems = function (filterText) {

            setTimeout(function () {

                if (filterText == '' || filterText == ' ') {
                    $scope.projects = allProjects;
                    $scope.ProjectsAlert = $scope.projects.length > 0 ? false : true;
                }
                else {
                    $scope.projects = allProjects.filter(function (item) {
                        return item.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    });
                    $scope.ProjectsAlert = $scope.projects.length > 0 ? false : true;
                }

                $scope.$apply();
            }, 100);
        };

        app.init();
    });

}]);

