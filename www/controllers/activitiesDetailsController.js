﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('activitiesDetailsController', ['$scope', '$rootScope', '$http', '$filter', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, $filter, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var initComments = true;
    var allComments = [];
    var myPhotoBrowserPopupDark;

    function ClearListData() {
        allComments = [];
        loading = false;
        CookieService.setCookie('event-comments-page-number', 1);
    }

    function GetEventDetails(eventId, showLoader, IsPullToRefresh) {
        var params = {
            Id: eventId,
            PageNumber: "1",
            PageSize: $rootScope.pageSize
        };

        $$('#activitiesDetailsMainMap').show();

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        appServices.CallService('activitiesDetails', "POST", "api/CategoryDetails/ViewEvent", params, function (res) {
            ClearListData();
            SpinnerPlugin.activityStop();

            if (res != null) {
                var defautImage = $filter("filter")(res.images, { isDefault: true })[0];
                res.rating = parseInt(res.rating / 2);
                res.userRating = parseInt(res.userRating / 2);
                res.municipality = typeof res.municipality != 'undefined' && res.municipality != null && res.municipality != '' && res.municipality != ' ' ? ' - ' + res.municipality : '';
                $scope.event = res;
                $scope.eventImage = typeof defautImage != 'undefined' && defautImage != null ? defautImage.imageUrl : 'img/pic.jpg';
                $scope.EventContainsImages = typeof res.images != 'undefined' && res.images.length > 0 ? true : false;
                $scope.eventFavoriteClass = typeof res.isFavourite != 'undefined' && res.isFavourite != null && res.isFavourite ? 'ionicons ion-android-favorite' : 'ionicons ion-android-favorite-outline';
                $scope.isEventFavorite = typeof res.isFavourite != 'undefined' && res.isFavourite != null && res.isFavourite ? true : false;
                $scope.eventContacts = !res.contactPhoneNumber && !res.contactGoogle && !res.contactWhatsUp && !res.contactFaceBook && !res.contactTwitter ? true : false;
                $scope.eventImages = res.images;
                $scope.userIsRegistered = false;
                $scope.userIsOwner = false;

                if (CookieService.getCookie('userLoggedIn')) {
                    $scope.userIsRegistered = true;
                    if (!res.isOwner) {
                        $scope.userIsOwner = true;
                    }
                }

                

                $scope.DestinationLatitude = res.latitude;
                $scope.DestinationLongitude = res.longitude;

                document.addEventListener("deviceready", function () {
                    var markers = [{ "title": res.name, "lat": res.latitude, "lng": res.longitude, "description": res.description }];
                    helpers.initMap('mapEvent', markers, 'activitiesDetails', '', function (map) {
                        $('#activitiesDetailsMainMap').hide();
                    });

                    navigator.geolocation.getCurrentPosition(function (position) {
                        $rootScope.firstAddress = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        $rootScope.secondAddress = new google.maps.LatLng(res.latitude, res.longitude);
                        $scope.currentLatitude = position.coords.latitude;
                        $scope.currentLongitude = position.coords.longitude;
                        $scope.DestinationLatitude = res.latitude;
                        $scope.DestinationLongitude = res.longitude;

                        var markers = [{ "title": res.name, "lat": res.latitude, "lng": res.longitude, "description": res.description }];
                        helpers.initMap('mapEvent', markers, 'activitiesDetails', '', function (map) {
                            $('#activitiesDetailsMainMap').hide();
                        });

                    }, function (PositionError) {
                        var error = PositionError;
                    }, { enableHighAccuracy: true });
                }, false);

                if (typeof res.images != 'undefined' && res.images.length > 0) {
                    var photos = res.images;
                    var photosArray = [];

                    angular.forEach(photos, function (photo) {
                        photo.imageUrl = photo.imageUrl ? photo.imageUrl : 'img/pic.jpg';
                        photo.imageUrl = photo.imageUrl != '' && photo.imageUrl != ' ' && photo.imageUrl.indexOf('img/') == -1 ? hostUrl + 'Uploads/' + photo.imageUrl : photo.imageUrl;

                        var photoObj = { url: photo.imageUrl, caption: res.name };
                        photosArray.push(photoObj);
                    });

                    if (photosArray.length > 0) {
                        myPhotoBrowserPopupDark = app.photoBrowser({
                            photos: photosArray,
                            theme: 'dark',
                            backLinkText: 'إغلاق',
                            lazyLoading: true,
                            ofText: 'من',
                            type: 'popup'
                        });
                    }
                }

                if (IsPullToRefresh) {
                    app.pullToRefreshDone();
                }

                GetCommentsForEvent(res);
            }
            else {
                $scope.eventImages = [];
                $scope.eventImage = 'img/pic.jpg';
                $scope.eventFavoriteClass = 'ionicons ion-android-favorite';
                $scope.isEventFavorite = false;

                if (IsPullToRefresh) {
                    app.pullToRefreshDone();
                }
            }
        });

        setTimeout(function () {
            $scope.$digest();
        }, fw7.DelayBeforeScopeApply);
    }

    function GetCommentsForEvent(event) {
        var pageNumber = 1;
        var pageSize = $rootScope.pageSize;

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        appServices.CallService('activitiesDetails', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + event.eventId + "/1", '', function (comments) {
            SpinnerPlugin.activityStop();

            if (comments && comments.length > 0) {
                app.attachInfiniteScroll('#divInfiniteEventDetails');

                angular.forEach(comments, function (comment) {
                    var isCommentOwner = false;
                    if (CookieService.getCookie('userLoggedIn')) {
                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                    }

                    comment.photoUrl = 'img/profile.png';
                    if (typeof comment.photoUrl != 'undefined' && comment.photoUrl != null && comment.photoUrl != '' && comment.photoUrl != ' ') {
                        comment.photoUrl = comment.photoUrl;
                    }

                    comment.isCommentOwner = isCommentOwner;
                    allComments.push(comment);
                });

                $scope.comments = allComments;
                $scope.EventContainsComments = true;
                $scope.EventMoreThanThreeComments = comments.length > 3 ? true : false;

                if (comments && comments.length < pageSize) {
                    $('#infiniteLoaderActivityDetails img').css('display', 'none');
                    app.detachInfiniteScroll('#divInfiniteEventDetails');
                    return;
                }

            }
            else {
                $scope.comments = [];
                $scope.EventContainsComments = false;
                $scope.EventMoreThanThreeComments = false;
            }
        });
    }

    InitService.addEventListener('ready', function () {
        $$(".overlayMap").click(function () {
            $$(".overlayMap").hide();
        });

        app.onPageInit('activitiesDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'activitiesDetails') return;
            $rootScope.currentOpeningPage = 'activitiesDetails';
           
            $$('#divInfiniteEventDetails').on('infinite', function () {
                if (loading) return;
                loading = true;

                var pageNumber = parseInt(CookieService.getCookie('event-comments-page-number')) + 1;
                var pageSize = $rootScope.pageSize;
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activitiesDetails', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + $scope.event.eventId + "/1", '', function (comments) {
                    SpinnerPlugin.activityStop();
                    if (comments && comments.length > 0) {
                        loading = false;
                        CookieService.setCookie('event-comments-page-number', parseInt(CookieService.getCookie('event-comments-page-number')) + 1);

                        angular.forEach(comments, function (comment) {
                            var isCommentOwner = false;
                            if (CookieService.getCookie('userLoggedIn')) {
                                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                            }
                            comment.isCommentOwner = isCommentOwner;
                            allComments.push(comment);
                        });

                        $scope.comments = allComments;
                        $scope.EventContainsComments = true;
                        $scope.EventMoreThanThreeComments = comments.length > 3 ? true : false;
                        if (comments && comments.length < pageSize) {
                            $('#infiniteLoaderActivityDetails img').css('display', 'none');
                            app.detachInfiniteScroll('#divInfiniteEventDetails');
                            return;
                        }
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            });

            $$('#divInfiniteEventDetails').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#activitiesDetailswiper');
                        app.pullToRefreshDone();
                    }
                });

            });
        });

        app.onPageReinit('activitiesDetails', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }

            $rootScope.initalSwiper('#activitiesDetailswiper');       
        });

        app.onPageBeforeAnimation('activitiesDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'activitiesDetails') return;
            $rootScope.currentOpeningPage = 'activitiesDetails';

            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = null;
            $scope.Advertisements = advertisments;

            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#activitiesDetailswiper');

            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('activitiesDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'activitiesDetails') return;
            $rootScope.currentOpeningPage = 'activitiesDetails';

            var eventId = page.query.eventId;
            var categoryId = page.query.categoryId;

            $scope.eventImages = [];
            $scope.eventFavoriteClass = 'ionicons ion-android-favorite';
            $scope.isEventFavorite = false;
            $scope.userIsRegistered = false;
            $scope.userIsOwner = false;
            $scope.categoryId = page.query.categoryId;

            GetEventDetails(eventId, true, false);

            $$('#divInfiniteEventDetails').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                GetEventDetails(eventId, false, true);
            });

            resetActivityDetailsForm();
        });

        $scope.submitForm = function (isValid) {
            $scope.EventCommentReset = true;
            if (isValid) {
                var params = {
                    'CategoryDetailsId': $scope.event.eventId
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activitiesDetails', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (result) {
                    if (result != null) {
                        var params = {
                            CommentText: $scope.eventCommentsForm.commentText,
                            CommentTypeId: 1,
                            ParentId: null,
                            RelatedId: $scope.event.eventId
                        };

                        appServices.CallService('activitiesDetails', 'POST', "api/Comments/SaveComment", params, function (result) {
                            SpinnerPlugin.activityStop();
                            if (result) {
                                resetActivityDetailsForm();
                                result.isCommentOwner = true;
                                $scope.comments.splice(0, 0, result);
                                $scope.EventContainsComments = true;
                                $scope.EventMoreThanThreeComments = $scope.comments.length > 3 ? true : false;

                                result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';

                                if (JSON.parse(CookieService.getCookie('userLoggedIn'))) {
                                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                    if (typeof userLoggedIn.photoUrl != 'undefined' && userLoggedIn.photoUrl != null && userLoggedIn.photoUrl != '' && userLoggedIn.photoUrl != ' ') {
                                        result.photoUrl = userLoggedIn.photoUrl;
                                    }
                                }
                            }

                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                        });
                    }
                });
            }
        };

        $scope.OpenReplies = function (comment) {
            CookieService.setCookie('requiredComment', JSON.stringify(comment));
            CookieService.setCookie('requiredEvent', JSON.stringify($scope.event));

            helpers.GoToPage('replies', null);
        };

        $scope.DeleteComment = function (comment) {
            language.openFrameworkModal('تأكيد', 'هل انت متأكد من حذف التعليق ؟', 'confrim', function () {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activitiesDetails', 'POST', "api/Comments/DeleteComment/" + comment.id, '', function (result) {
                    SpinnerPlugin.activityStop();
                    if (result != null) {
                        $scope.comments = $scope.comments.filter(function (item) {
                            return item.id !== comment.id;
                        });

                        $scope.EventContainsComments = $scope.comments != null && $scope.comments.length > 0 ? true : false;
                        $scope.EventMoreThanThreeComments = $scope.comments != null && $scope.comments.length > 3 ? true : false;
             
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            });
        };

        $scope.filterImages = function (item) {
            return true;
        };

        $scope.RateEvent = function (rate, event) {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            var params = {
                'CategoryDetailsId': event.eventId
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('activitiesDetails', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (result) {
                if (result != null) {
                    var ratingParams = {
                        "RateValues": "1:" + parseInt(rate * 2),
                        "RelatedId": event.eventId,
                        "UserId": userLoggedIn.id,
                        "RelatedType": 1,
                        "Description": parseInt(rate * 2)
                    };

                    appServices.CallService('activitiesDetails', 'POST', "api/Rating/SaveRating", ratingParams, function (ratingResult) {
                        SpinnerPlugin.activityStop();

                        if (ratingResult && ratingResult == 1) {
                            language.openFrameworkModal('نجاح', 'تم اضافة التقييم بنجاح', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', 'خطا اثناء اضافة التقييم', 'alert', function () { });
                        }
                    });
                }
            });

        };

        $scope.shareEvent = function (event) {
            var url = 'http://JwlaClient.saned-projects.com/Home/index/' + event.eventId;

            window.plugins.socialsharing.share('', event.name, [''], url, function (result) {
                console.log('result: ' + result);
            },
          function (msg) {
              console.log("Sharing failed with message: " + msg);
          });
        }

        $scope.openEventGallery=function(index){
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.addEventInFavorites = function (event) {
            var isFavourite = $scope.isEventFavorite;
            var params = {};

            var MethodName = typeof isFavourite != 'undefined' && isFavourite != null && isFavourite ? 'api/Favourite/RemoveFavourite/' + event.eventId : 'api/Favourite/CreateFavourite';
            if (!isFavourite) {
                params = {
                    CategoryDetailsId: event.eventId
                };
            }

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('activitiesDetails', 'POST', MethodName, params, function (response) {
                SpinnerPlugin.activityStop();

                if (response) {
                    $scope.eventFavoriteClass = typeof isFavourite != 'undefined' && isFavourite != null && isFavourite ? 'ionicons ion-android-favorite-outline' : 'ionicons ion-android-favorite';
                    $scope.isEventFavorite = typeof isFavourite != 'undefined' && isFavourite != null && isFavourite ? false : true;
                }
            });
        };

        $scope.form = {};
        $scope.eventCommentsForm = {};
        $scope.eventRepliesForm = {};
        $scope.replies = [];
        $scope.CommentContainsReplies = false;

        var resetActivityDetailsForm = function () {
            $scope.EventCommentReset = false;
            $scope.eventCommentsForm.commentText = null;
            if (typeof $scope.EventCommentsForm != 'undefined' && $scope.EventCommentsForm != null) {
                $scope.EventCommentsForm.$setPristine(true);
                $scope.EventCommentsForm.$setUntouched();
            }
        };

        $scope.openEventMap = function () {
            launchnavigator.navigate([$scope.DestinationLatitude, $scope.DestinationLongitude], {
                start: [$scope.currentLatitude, $scope.currentLongitude]
            });
        }

        $scope.matchSocial = function (socialLink) {
            if ((socialLink).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/))
                cordova.InAppBrowser.open(socialLink, '_system', 'location=no');
            else
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
        }

        $scope.eventOwnerContact = function (contactType, object) {
            if (contactType == "call") {
                window.plugins.CallNumber.callNumber(
               function onSuccess(successResult) {
                   console.log("Success:" + successResult);
               }, function onError(errorResult) {
                   console.log("Error:" + errorResult);
               }, object, true);
            }
            else if (contactType == "whatsapp") {
                window.open('whatsapp://send?= مرحبا اخى&phone=+966' + object, "_system");
            }
            else if (contactType == "facebok") {
                $scope.matchSocial(object);
            }
            else if (contactType == "twitter") {
                $scope.matchSocial(object);
            }
            else if (contactType == "googleplus") {
                $scope.matchSocial(object);
            }
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
        };

        $scope.GoToUber = function (event) {
            language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
        };

        $scope.GoToCareem = function (event) {
            language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
        };

        $scope.GoToCaptain = function (place) {
            language.openFrameworkModal('خطأ', 'هذه الخدمة غير متوفرة حاليا .', 'alert', function () { });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

