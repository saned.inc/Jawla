﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('changePasswordController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';
        });

        app.onPageBeforeAnimation('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';
            resetChangePasswordForm();
        });

        app.onPageAfterAnimation('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';

            resetChangePasswordForm();

            setTimeout(function () {
                $scope.$digest();
            }, fw7.DelayBeforeScopeApply);
        });

        var resetChangePasswordForm = function () {
            $scope.changePasswordReset = false;
            $scope.changePassForm.oldPassword = null;
            $scope.changePassForm.newPassword = null;
            $scope.changePassForm.confrmNewPassword = null;
            if (typeof $scope.$parent.ChangePasswordForm != 'undefined' && $scope.$parent.ChangePasswordForm != null) {
                $scope.$parent.ChangePasswordForm.$setPristine(true);
                $scope.$parent.ChangePasswordForm.$setUntouched();
            }
        }

        $scope.form = {};
        $scope.changePassForm = {};

        $scope.submitForm = function (isValid) {
            $scope.changePasswordReset = true;
            if (isValid) {
                var params = {
                    'oldPassword': $scope.changePassForm.oldPassword,
                    'newPassword': $scope.changePassForm.newPassword,
                    'ConfirmPassword': $scope.changePassForm.confrmNewPassword
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('changePass', "POST", "Api/Account/ChangePassword", params, function (res) {
                    SpinnerPlugin.activityStop();

                    if (res != null) {
                        language.openFrameworkModal('نجاح', 'تم تعديل كلمة السر بنجاح .', 'alert', function () { });
                        helpers.GoToPage('login', null);
                    }
                });
                
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        app.init();
    });

}]);

