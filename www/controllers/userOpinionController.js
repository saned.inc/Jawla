﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userOpinionController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var initComments = true;
    var isCommentOwner = false;
    var initPageCovered = false;
    $scope.CommentContainsReplies = false;
    var visitorCoverId;
    $scope.form = {};
    $scope.userOpinionCommentsForm = {};
    $scope.userOpinionRepliesForm = {};
    var allComments = [];
    $scope.replies = [];
    var myPhotoBrowserPopupDark;
    

    function ClearListData() {
        allComments = [];
        loading = false;
        CookieService.setCookie('userOpinion-comments-page-number', 1);
        $scope.userOpinionAlert = false;
    }

    function getImageFromGallery() {
        navigator.camera.getPicture(UploadPhotoFromGallery, function (message) {

        }, {
            quality: 100,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        });
    }

    function getImageFromCamera() {
        navigator.camera.getPicture(UploadPhotoFromCamera, function (message) {

        }, {
            quality: 100,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        });
    }

    function UploadPhotoFromGallery(imageURI) {
        SavePhoto(imageURI);
    }

    function UploadPhotoFromCamera(imageURI) {
        SavePhoto(imageURI);
    }

    function SavePhoto(imageURI) {
        var newImages = [];
        newImages.push(imageURI);

        var params = {
            'CategoryDetailsId': visitorCoverId,
            'Images': newImages
        };

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        appServices.CallService('userOpinion', "POST", "api/VisitorCover/CreateCoverPhotos", params, function (res) {
            SpinnerPlugin.activityStop();
            if (res != null) {
                language.openFrameworkModal('نجاح', 'تم رفع الصورة بنجاح .', 'alert', function () {

                    GetPhotos($scope.userOpinion.categoryDetailsId, function (result) {
                        if (result != null) {
                            GetPhotosInGallery();
                        }

                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    });
                });
            }
            else {
                language.openFrameworkModal('خطأ', 'خطأ في إضافة الصورة.', 'alert', function () { });
            }
        });
    }

    function GetPhotos(categoryDetailsId, callBack) {
        var params = {
            'CategoryDetailsId': categoryDetailsId
        };

        appServices.CallService('userOpinion', "POST", "api/VisitorCover/ViewAllPhotoCover", params, function (result) {
            if (result != null) {
                $scope.UserOpinionContainsImages = typeof result != 'undefined' && result.length > 0 ? true : false;
                $scope.UserOpinionContainsFourImages = typeof result != 'undefined' && result.length > 3 ? true : false;
                $scope.userOpinionImages = result;
                setTimeout(function () {
                    $scope.$digest();
                }, fw7.DelayBeforeScopeApply);
                callBack(result);
            }
            else {
                callBack(null);
            }
        });
    }

    function GetPhotosInGallery() {
        var name = $scope.userOpinion.name;
        var photos = $scope.userOpinionImages;
        var photosArray = [];

        angular.forEach(photos, function (photo) {
            photo.photoUrl = photo.photoUrl ? photo.photoUrl : 'img/pic.jpg';
            var photoObj = { url: hostUrl + 'Uploads/' + photo.photoUrl, caption: name };
            photosArray.push(photoObj);
        });

        if (photosArray.length > 0) {
            myPhotoBrowserPopupDark = app.photoBrowser({
                photos: photosArray,
                theme: 'dark',
                backLinkText: 'إغلاق',
                lazyLoading:true,
                ofText: 'من',
                type: 'popup'
            });
        }
    }

    InitService.addEventListener('ready', function () {
        app.onPageInit('userOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'userOpinion') return;
            $rootScope.currentOpeningPage = 'userOpinion';
           

            $$('#divInfiniteUserOpinion').on('infinite', function () {
                if (loading) return;
                loading = true;
                $scope.UserOpinionMoreThanThreeComments = true;

                var pageNumber = parseInt(CookieService.getCookie('userOpinion-comments-page-number')) + 1;
                var pageSize = $rootScope.pageSize;
                if (initPageCovered == false) {

                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    appServices.CallService('userOpinion', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + visitorCoverId + "/1", '', function (comments) {
                        SpinnerPlugin.activityStop();
                        if (comments && comments.length > 0) {
                            loading = false;
                            CookieService.setCookie('userOpinion-comments-page-number', parseInt(CookieService.getCookie('userOpinion-comments-page-number')) + 1);

                            angular.forEach(comments, function (comment) {
                                isCommentOwner = false;
                                if (CookieService.getCookie('userLoggedIn')) {
                                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                    comment.fullName = comment.fullName ? comment.fullName : comment.userName;
                                    isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                                }
                                comment.isCommentOwner = isCommentOwner;
                                allComments.push(comment);
                            });
                            $scope.comments = allComments;
                            $scope.userOpinionAlert = false;
                            $scope.UserOpinionContainsComments = true;
                            $scope.UserOpinionMoreThanThreeComments = comments.length > $rootScope.pageSize ? true : false;
                            if (comments && comments.length < pageSize) {
                                $('#infiniteLoaderUserOpinion img').css('display', 'none');
                                app.detachInfiniteScroll('#divInfiniteUserOpinion');
                                return;
                            }
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                        }
                        else {
                            $scope.userOpinionAlert = true;
                            $scope.UserOpinionMoreThanThreeComments = false;
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                        }
                    });
                }
                else {
                    $scope.userOpinionAlert = true;
                    $scope.UserOpinionMoreThanThreeComments = false;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
             
            });

            $$('#divInfiniteUserOpinion').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#UserOpinionSwiper');
                        app.pullToRefreshDone();

                    }
                });
  
            });

        });

        app.onPageBeforeAnimation('userOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'userOpinion') return;
            $rootScope.currentOpeningPage = 'userOpinion';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#UserOpinionSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;

        });

        app.onPageAfterAnimation('userOpinion', function (page) {
            if ($rootScope.currentOpeningPage != 'userOpinion') return;
            $rootScope.currentOpeningPage = 'userOpinion';
            

             visitorCoverId = page.query.visitorCoverId;
            $scope.userOpinionAlert = false;
            $scope.userOpinionImages = [];
            $scope.userOpinionDeletedImages = [];
            $scope.userIsRegistered = false;
            $scope.userClicksRemove = false;
            $scope.removeImageButtonText = 'حذف صور';
            $scope.UserOpinionContainsImages = false;
            $scope.UserOpinionContainsFourImages = false;

            GetUserOpinionDetails(visitorCoverId, true, false);

            var divInfiniteUserOpinion = $$('#divInfiniteUserOpinion');

            divInfiniteUserOpinion.on('ptr:refresh', function (e) {
                $rootScope.load = true;
                GetUserOpinionDetails(visitorCoverId, false, true);
            });

            resetUserOpinionForm();
        });

        app.onPageReinit('userOpinion', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#UserOpinionSwiper');

            visitorCoverId = page.query.visitorCoverId;
            GetUserOpinionDetails(visitorCoverId, true, false);

        });

        function GetUserOpinionDetails(visitorCoverId, showLoader, IsPullToRefresh) {
            var params = {
                Id: visitorCoverId
            };

            $('div.delOverlay').addClass('overlayIsHidden');

            if (showLoader) {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            }
            appServices.CallService('userOpinion', "POST", "api/VisitorCover/GetCoverDetails", params, function (res) {
                ClearListData();
                if (res != null) {
                    initPageCovered = true;
                    res.rating = parseInt(res.rating / 2);
                   res.userRating = parseInt(res.userRating / 2);

                    $scope.userOpinion = res;
                    $scope.userIsRegistered = false;
                    if (CookieService.getCookie('userLoggedIn')) {
                        $scope.userIsRegistered = true;
                    }

                    GetPhotos(res.categoryDetailsId, function (result) {
                        if (result != null) {
                            GetPhotosInGallery();
                        }

                        if (showLoader) {
                            SpinnerPlugin.activityStop();
                        }
                        if (IsPullToRefresh) {
                            app.pullToRefreshDone();
                        }
                        GetCommentsForUserOpinion(res);
                    });

                }
                else {
                    $scope.userOpinionImages = [];
                    $scope.userOpinion = null;
                    if (showLoader) {
                        SpinnerPlugin.activityStop();
                    }
                    if (IsPullToRefresh) {
                        app.pullToRefreshDone();
                    }
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });

           
        }

        function GetCommentsForUserOpinion(userOpinion) {
            var pageNumber = 1;
            var pageSize = $rootScope.pageSize;
            $scope.UserOpinionMoreThanThreeComments = true;

            appServices.CallService('userOpinion', 'GET', "api/Comments/GetPagedComments/" + pageNumber + "/" + pageSize + "/" + userOpinion.categoryDetailsId + "/1", '', function (comments) {
                SpinnerPlugin.activityStop();
                if (comments && comments.length > 0) {
                    app.attachInfiniteScroll('#divInfiniteUserOpinion');

                    angular.forEach(comments, function (comment) {
                        isCommentOwner = false;
                        if (CookieService.getCookie('userLoggedIn')) {
                            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                            comment.fullName = comment.fullName ? comment.fullName : comment.userName;
                            isCommentOwner = userLoggedIn.id == comment.userId ? true : false;
                        }

                        if (typeof comment.photoUrl != 'undefined' && comment.photoUrl != null && comment.photoUrl != '' && comment.photoUrl != ' ') {
                            comment.photoUrl = comment.photoUrl;
                        }
                        else {
                            comment.photoUrl = 'img/profile.png';
                        }

                        comment.isCommentOwner = isCommentOwner;
                        allComments.push(comment);
                    });
                    $scope.comments = allComments;
                    $scope.userOpinionAlert = false;
                    $scope.UserOpinionContainsComments = true;
                    $scope.UserOpinionMoreThanThreeComments = comments.length > 3 ? true : false;
                    if (comments && comments.length < pageSize) {
                        $('#infiniteLoaderUserOpinion img').css('display', 'none');
                        app.detachInfiniteScroll('#divInfiniteUserOpinion');
                        return;
                    }
                    SpinnerPlugin.activityStop();
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.comments = [];
                    $scope.userOpinionAlert = true;
                    $scope.UserOpinionContainsComments = false;
                    $scope.UserOpinionMoreThanThreeComments = false;
                    SpinnerPlugin.activityStop();
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });
        }

        $scope.submitForm = function (isValid) {
            $scope.UserOpinionCommentReset = true;
            if (isValid) {
                if (CookieService.getCookie('userLoggedIn')) { 
                var params = {
                    'CategoryDetailsId': $scope.userOpinion.categoryDetailsId
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('userOpinion', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (res) {
                    if (res != null) {
                        var params = {
                            CommentText: $scope.userOpinionCommentsForm.commentText,
                            CommentTypeId: 1,
                            ParentId: null,
                            RelatedId: $scope.userOpinion.categoryDetailsId
                        };

                        appServices.CallService('userOpinion', 'POST', "api/Comments/SaveComment", params, function (result) {
                            SpinnerPlugin.activityStop();
                            if (result) {
                                resetUserOpinionForm();
                                SpinnerPlugin.activityStop();

                                if (CookieService.getCookie('userLoggedIn')) {
                                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                    result.fullName = result.fullName ? result.fullName : result.userName;
                                    if (typeof userLoggedIn.photoUrl != 'undefined' && userLoggedIn.photoUrl != null && userLoggedIn.photoUrl != '' && userLoggedIn.photoUrl != ' ') {
                                        result.photoUrl = userLoggedIn.photoUrl;
                                    }
                                    else {
                                        result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';
                                    }
                                    
                                    isCommentOwner = userLoggedIn.id == result.userId ? true : false;
                                    result.isCommentOwner = isCommentOwner;
                                }
                                else {
                                    result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';
                                }
                                $scope.comments.splice(0, 0, result);
                                $scope.userOpinionAlert = false;
                                $scope.UserOpinionContainsComments = true;
                                $scope.UserOpinionMoreThanThreeComments = $scope.comments.length > 3 ? true : false;
                                setTimeout(function () {
                                    $scope.$digest();
                                }, fw7.DelayBeforeScopeApply);
                            }
                            else {
                                SpinnerPlugin.activityStop();
                                setTimeout(function () {
                                    $scope.$digest();
                                }, fw7.DelayBeforeScopeApply);
                            }
                        });
                    }
                });
            }
                else {
                    SpinnerPlugin.activityStop();
                    language.openFrameworkModal('تنبيه', 'قم بتسجيل الدخول اولا', 'alert', function () { });
            }  
          }
        };

        $scope.OpenReplies = function (comment) {
            CookieService.setCookie('requiredComment', JSON.stringify(comment));
            CookieService.setCookie('requiredEvent', JSON.stringify($scope.userOpinion));

            helpers.GoToPage('replies', null);
        };

        $scope.DeleteComment = function (comment) {
            language.openFrameworkModal('تأكيد', 'هل انت متأكد من حذف التعليق ؟', 'confrim', function () {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('userOpinion', 'POST', "api/Comments/DeleteComment/" + comment.id, '', function (result) {
                    SpinnerPlugin.activityStop();
                    if (result != null) {
                        $scope.comments = $scope.comments.filter(function (item) {
                            return item.id !== comment.id;
                        });

                        $scope.UserOpinionContainsComments = $scope.comments != null && $scope.comments.length > 0 ? true : false;
                        $scope.UserOpinionMoreThanThreeComments = $scope.comments != null && $scope.comments.length > 3 ? true : false;
                        SpinnerPlugin.activityStop();
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            });
        };

        $scope.RateUserOpinion = function (rate, userOpinion) {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            var params = {
                'CategoryDetailsId': userOpinion.categoryDetailsId
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('userOpinion', 'POST', "api/VisitorCover/CreateVisitorCover", params, function (result) {
                if (result != null) {
                    var ratingParams = {
                        "RateValues": "1:" + parseInt(rate * 2),
                        "RelatedId": userOpinion.categoryDetailsId,
                        "UserId": userLoggedIn.id,
                        "RelatedType": 1,
                        "Description": parseInt(rate * 2)
                    };

                    appServices.CallService('userOpinion', 'POST', "api/Rating/SaveRating", ratingParams, function (ratingResult) {
                        SpinnerPlugin.activityStop();
                        if (ratingResult && ratingResult == 1) {
                            language.openFrameworkModal('نجاح', 'تم اضافة التقييم بنجاح', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', 'خطا اثناء اضافة التقييم', 'alert', function () { });
                        }
                    });
                }
            });
        };

        var resetUserOpinionForm = function () {
            $scope.UserOpinionCommentReset = false;
            $scope.userOpinionCommentsForm.commentText = null;
            if (typeof $scope.UserOpinionCommentsForm != 'undefined' && $scope.UserOpinionCommentsForm != null) {
                $scope.UserOpinionCommentsForm.$setPristine(true);
                $scope.UserOpinionCommentsForm.$setUntouched();
            }
        };

        $scope.ClickRemoveImages = function () {
            var userOpinionDeletedImages = $scope.userOpinionDeletedImages;
            if (userOpinionDeletedImages != null && userOpinionDeletedImages.length > 0) {
                language.openFrameworkModal('تأكيد', 'هل أنت متأكد من حذف الصور ؟', 'confirm', function () {
                    var IdsToDelete = '';
                    var counter = 1;

                    angular.forEach(userOpinionDeletedImages, function (userImageToDelete) {
                        if (IdsToDelete == '') {
                            IdsToDelete = userImageToDelete.id;
                        }
                        else {
                            IdsToDelete += ',' + userImageToDelete.id;
                        }

                        if (counter == userOpinionDeletedImages.length) {
                            var params = {
                                'Ids': IdsToDelete,
                                'CategoryDetailsId': $scope.userOpinion.categoryDetailsId
                            };

                            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                            appServices.CallService('userOpinion', 'POST', "api/VisitorCover/DeletePhotoCoverByIds", params, function (result) {
                                SpinnerPlugin.activityStop();
                                if (result != null) {
                                    $scope.userOpinionImages = $scope.userOpinionImages.filter(function (x) {
                                        return $scope.userOpinionDeletedImages.indexOf(x) < 0
                                    })
                                    $scope.userClicksRemove = $scope.userClicksRemove == true ? false : true;
                                    $scope.removeImageButtonText = $scope.userClicksRemove == true ? 'تم' : 'حذف الصور';
                                    $scope.UserOpinionContainsFourImages = $scope.userOpinionImages.length > 3 ? true : false;
                                    $scope.UserOpinionContainsImages = $scope.userOpinionImages.length > 0 ? true : false;
                                    $scope.userOpinionDeletedImages = [];
                                    IdsToDelete = '';

                                    SpinnerPlugin.activityStop();
                                    setTimeout(function () {
                                        $scope.$digest();
                                    }, fw7.DelayBeforeScopeApply);
                                }
                                else {
                                    SpinnerPlugin.activityStop();
                                }
                            });
                        }
                        else {
                            counter++;
                        }
                    });


                });
            }
            else {
                $scope.userClicksRemove = $scope.userClicksRemove == true ? false : true;
                $scope.removeImageButtonText = $scope.userClicksRemove == true ? 'تم' : 'حذف الصور';

                if ($scope.userClicksRemove == true) {
                    $('div.delOverlay').removeClass('overlayIsHidden');
                    angular.forEach($scope.userOpinionImages, function (userOpinionImage) {
                        $scope.userOpinionDeletedImages.push(userOpinionImage);
                    });
                }
                else {
                    $scope.userOpinionDeletedImages = [];
                    $('div.delOverlay').addClass('overlayIsHidden');
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
        };

        $scope.openUserOpinionGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.AddImageToDeletedList = function ($event, imageIndex, userOpinionImage) {
            if ($scope.userClicksRemove == true) {
                var element = angular.element($event.currentTarget)[0];
                $(element).find('div.delOverlay').each(function (index) {
                    var overlayElement = $(this);
                    if ($(this).hasClass('overlayIsHidden')) {
                        $(this).removeClass('overlayIsHidden');
                        $scope.userOpinionDeletedImages.push(userOpinionImage);
                    }
                    else {
                        $(this).addClass('overlayIsHidden');
                        $scope.userOpinionDeletedImages = $scope.userOpinionDeletedImages.filter(function (item) {
                            return item.photoUrl !== userOpinionImage.photoUrl;
                        });
                    }
                });
            }
        };

        $scope.AddImageFromGallery = function () {
            getImageFromGallery();
        };

        $scope.AddImageFromCamera = function () {
            getImageFromCamera();
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
         
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

