﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('subCategoriesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var categoryId;


    function LoadSubCategories(categoryId, callBack) {
        appServices.CallService('home', 'GET', "api/Category/GetCategoryByParentId/" + categoryId, '', function (subCategories) {
            if (subCategories) {
                $scope.subCategories = subCategories;
                callBack(true);
            }
        });
    }

    function LoadAllPublicData(categoryId, callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        LoadSubCategories(categoryId, function (IsCategoriesLoaded) {
            SpinnerPlugin.activityStop();
            callBack(true);
        });
    }

    function LoadData(categoryId) {
        $scope.currentLocation = $rootScope.currentLocation;
        $scope.selectedItem = $rootScope.currentLocation;
        $scope.selectedItem = $rootScope.CurrentuserCity;
        LoadAllPublicData(categoryId, function (result) {
            if (result != null && result == true) {
                var advertisments = myApp.fw7.Advertisements;
                $scope.Advertisements = advertisments;

                if (advertisments !== undefined && advertisments.length > 0) {
                    $rootScope.initalSwiper('#subCategorySwiper');
                }
            }

        });
        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $document.ready(function () {
        app.onPageInit('subCategories', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategories') return;
            $rootScope.currentOpeningPage = 'subCategories';
            var categoryId = page.query.categoryId;

            SidePanelService.DrawMenu();

            $$('#divInfiniteSubCategriesPullToRefresh').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#subCategorySwiper');
                        app.pullToRefreshDone();
                    }
                });

            });
            LoadData(categoryId);

        });


        app.onPageReinit('subCategories', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#subCategorySwiper');
            var categoryId = page.query.categoryId;
            $scope.cites = myApp.fw7.Cities;
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            //LoadData(categoryId);

            $$('img.lazy').trigger('lazy');
        });

        app.onPageBeforeAnimation('subCategories', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategories') return;
            $rootScope.currentOpeningPage = 'subCategories';
            categoryId = page.query.categoryId;
            $scope.cites = myApp.fw7.Cities;
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            LoadData(categoryId);

            $$('img.lazy').trigger('lazy');
        });

        app.onPageAfterAnimation('subCategories', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategories') return;
            $rootScope.currentOpeningPage = 'subCategories';

            $$('img.lazy').trigger('lazy');
        });

        $scope.goToCategoryDetails = function (subCategory) {
            if (categoryId < 5) {
                helpers.GoToPage('places', { categoryId: categoryId, subCategoryId: subCategory.id, subCategoryName: subCategory.name });
            }
            else if (categoryId == 5) {
                helpers.GoToPage('events', { categoryId: categoryId, subCategoryId: subCategory.id, subCategoryName: subCategory.name });
            }
            else if (categoryId == 6) {
                helpers.GoToPage('projects', { categoryId: categoryId, subCategoryId: subCategory.id, subCategoryName: subCategory.name });
            }
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;

        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

