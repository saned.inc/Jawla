﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('loginController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';
        });

        app.onPageReinit('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

            $scope.resetForm();
        });

        app.onPageBeforeAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

        });

        app.onPageAfterAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

            $scope.resetForm();

        });
        app.init();
    });

    $scope.resetForm = function () {
        $scope.loginReset = false;
        $scope.loginForm.username = null;
        $scope.loginForm.password = null;
        if (typeof $scope.LoginForm != 'undefined' && $scope.LoginForm != null) {
            $scope.LoginForm.$setPristine(true);
            $scope.LoginForm.$setUntouched();
        }
    }

    function RegisterDevice(callBack) {
        helpers.RegisterDevice(function (result) {
            callBack(true);
        });
        callBack(true);
    }

    $scope.PassToHome = function () {

        RegisterDevice(function (result) {
            CookieService.setCookie('Visitor', true);
            SidePanelService.DrawMenu();
            helpers.GoToPage('home', null);
       });
    };

    $scope.GoToSignUp = function () {
        helpers.GoToPage('signup', null);
    };

    $scope.GoToForgetPassword = function () {
        helpers.GoToPage('forgetPass', null);
    };
    $scope.form = {};
    $scope.loginForm = {};
    $scope.submitForm = function (isValid) {
        $scope.loginReset = true;
        if (isValid) {

            var loginEmail = $scope.loginForm.username, loginPass = $scope.loginForm.password;

            loginEmail = loginEmail.trim();

            if ((loginEmail != '' && loginEmail != ' ' && loginEmail != null) && loginPass != null) {
                CookieService.setCookie('USName', loginEmail);
                CookieService.setCookie('USPassword', loginPass);
              RegisterDevice(function (result) {
                    appServices.GetToken('login', "POST", "token", loginEmail, loginPass, function (res) {
                        if (res != null) {
                            CookieService.setCookie('appToken', res.access_token);
                            CookieService.setCookie('USName', res.userName);
                            CookieService.setCookie('refreshToken', res.refresh_token);
                            CookieService.setCookie('Visitor', false);
                            
                            CookieService.setCookie('loginUsingSocial', false);
                            appServices.CallService('login', "POST", "api/Account/GetUserInfo", '', function (res) {
                                SpinnerPlugin.activityStop();
                                if (res != null) {
                                    if (res.isAmbassador) {
                                        $rootScope.isAmbassador = true;
                                    }
                                    initUserBlocked = false;
                                    CookieService.setCookie('userLoggedIn', JSON.stringify(res.user));
                                    SidePanelService.DrawMenu();
                                    if (typeof res.user.photoUrl != 'undefined' && res.user.photoUrl != null && res.user.photoUrl != '' && res.user.photoUrl != ' ') {
                                        CookieService.setCookie('usrPhoto', res.user.photoUrl);
                                    }
                                    else {
                                        CookieService.removeCookie('usrPhoto');
                                    }
                                    helpers.GoToPage('home', null);

                                }
                            });
                        }
                        else {
                            SpinnerPlugin.activityStop();
                        }
                    });
              });
            }
            else {
                language.openFrameworkModal('تنبيه', 'من فضلك أدخل إسم الدخول وكلمة المرور', 'alert', function () { });
            }
        }
    };

    $scope.loginWithFacebook = function () {
        var fbLoginFailed = function (error) {
            
        }

        var fbLoginSuccess = function (userData) {
            if (userData.authResponse) {

                var accessToken = userData.authResponse.accessToken;
                var userFullName = '';
                var userEmail = '';

                facebookConnectPlugin.api(userData.authResponse.userID + "/?fields=id,email,first_name,last_name", ["public_profile"],
                    function (result) {
                        userFullName = result.first_name + ' ' + result.last_name;

                        CookieService.setCookie('socialEmail', result.email);
                        var registerObj = {};
                        if (CookieService.getCookie('FacebookObject')) {
                            registerObj = JSON.parse(CookieService.getCookie('FacebookObject'));
                        }
                        else {
                            registerObj = {
                                "Provider": "Facebook",
                                "userId": userData.authResponse.userID,
                                "name": userFullName,
                                'email': result.email,
                                "ExternalAccessToken": accessToken
                            };
                        }

                        RegisterDevice(function (result) {
                            appServices.CallService('login', "POST", "api/Account/RegisterExternal", registerObj, function (res) {
                                SpinnerPlugin.activityStop();
                                CookieService.removeCookie('FacebookObject');
                                if (res != null) {
                                    CookieService.setCookie('USName', res.userName);
                                    CookieService.setCookie('appToken', res.access_token);
                                    CookieService.setCookie('Visitor', false);
                                    CookieService.setCookie('loginUsingSocial', true);
                                    CookieService.setCookie('FacebookObject', JSON.stringify(registerObj));
                                    appServices.CallService('login', "POST", "api/Account/GetUserInfo", '', function (res1) {
                                        SpinnerPlugin.activityStop();
                                        if (res1 != null) {
                                            res1.user.userName = res1.user.name;
                                            CookieService.setCookie('userLoggedIn', JSON.stringify(res1.user));
                                            if (typeof res1.user.photoUrl != 'undefined' && res1.user.photoUrl != null && res1.user.photoUrl != '' && res1.user.photoUrl != ' ') {
                                                CookieService.setCookie('usrPhoto', res1.user.photoUrl);
                                            }
                                            else {
                                                CookieService.removeCookie('usrPhoto');
                                            }
                                            helpers.GoToPage('home', null);
                                        }
                                    });
                                }
                            });
                        });
                    },
                    function (error) {
                        console.log('ERROR:' + error);
                    });
            }
        }

        facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess, fbLoginFailed);

    };

    $scope.loginWithTwitter = function () {
        TwitterConnect.login(function (data) {
            var accessToken = data.token;
            var userFullName = '';

            TwitterConnect.showUser(function (result) {
                var userFullName = result.name;
                var registerObj = {};
                if (CookieService.getCookie('TwitterObject')) {
                    registerObj = JSON.parse(CookieService.getCookie('TwitterObject'));
                }
                else {
                    registerObj = {
                        "Provider": "Twitter",
                        "userId": data.userId,
                        "name": userFullName,
                        'email': '',
                        "ExternalAccessToken": accessToken
                    };
                }
                RegisterDevice(function (result) {
                    appServices.CallService('login', "POST", "api/Account/RegisterExternal", registerObj, function (res) {
                        SpinnerPlugin.activityStop();
                        CookieService.removeCookie('TwitterObject');
                        if (res != null) {
                            CookieService.setCookie('USName', res.userName);
                            CookieService.setCookie('appToken', res.access_token);
                            CookieService.setCookie('Visitor', false);
                            CookieService.setCookie('loginUsingSocial', true);
                            CookieService.setCookie('TwitterObject', JSON.stringify(registerObj));
                            appServices.CallService('login', "POST", "api/Account/GetUserInfo", '', function (res1) {
                                SpinnerPlugin.activityStop();
                                if (res1 != null) {
                                    initUserBlocked = false;
                                    res1.user.userName = res1.user.name;
                                    CookieService.setCookie('userLoggedIn', JSON.stringify(res1.user));
                                    if (typeof res1.user.photoUrl != 'undefined' && res1.user.photoUrl != null && res1.user.photoUrl != '' && res1.user.photoUrl != ' ') {
                                        CookieService.setCookie('usrPhoto', res1.user.photoUrl);
                                    }
                                    else {
                                        CookieService.removeCookie('usrPhoto');
                                    }
                                    SpinnerPlugin.activityStop();
                                    helpers.GoToPage('home', null);

                                }
                            });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                });

            }, function (error) {
                language.openFrameworkModal('خطأ', 'خطأ في إسترجاع بيانات المستخدم', 'alert', function () { });
            });



        }, function (error) {
            console.log('Error in Login');
        });

    };

    $scope.loginWithGooglePlus = function () {
        window.plugins.googleplus.login(
                {
                    'scopes': 'profile email',
                    'webClientId': '280862617951-3jnfvkujfhkkpmn1ra93792eips9o23v.apps.googleusercontent.com',
                    'offline': false,
                },
                function (obj) {
                    var userName = obj.displayName;
                    var userEmail = obj.email;
                    var userId = obj.userId;
                    var userToken = obj.idToken;

                    CookieService.setCookie('socialEmail', userEmail);

                    var registerObj = {};
                    if (CookieService.getCookie('GoogleObject')) {
                        registerObj = JSON.parse(CookieService.getCookie('GoogleObject'));
                    }
                    else {
                        registerObj = {
                            "Provider": "Google",
                            "userId": userId,
                            "name": userName,
                            "email": userEmail,
                            "ExternalAccessToken": userToken
                        };
                    }

                    RegisterDevice(function (result) {
                        appServices.CallService('login', "POST", "api/Account/RegisterExternal", registerObj, function (res) {
                            SpinnerPlugin.activityStop();
                            CookieService.removeCookie('GoogleObject');
                            if (res != null) {
                                CookieService.setCookie('USName', res.userName);
                                CookieService.setCookie('appToken', res.access_token);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginUsingSocial', true);
                                CookieService.setCookie('GoogleObject', JSON.stringify(registerObj));
                                appServices.CallService('login', "POST", "api/Account/GetUserInfo", '', function (res1) {
                                    SpinnerPlugin.activityStop();
                                    if (res1 != null) {
                                        initUserBlocked = false;
                                        res1.user.userName = res1.user.name;
                                        CookieService.setCookie('userLoggedIn', JSON.stringify(res1.user));
                                        if (typeof res1.user.photoUrl != 'undefined' && res1.user.photoUrl != null && res1.user.photoUrl != '' && res1.user.photoUrl != ' ') {
                                            CookieService.setCookie('usrPhoto', res1.user.photoUrl);
                                        }
                                        else {
                                            CookieService.removeCookie('usrPhoto');
                                        }
                                        SpinnerPlugin.activityStop();
                                        helpers.GoToPage('home', null);

                                    }
                                });
                            }
                        });
                    });
                },
                function (msg) {
                    console.log('error: ' + msg);
                });
    };
}]);

