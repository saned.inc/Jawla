﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userDetailsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function getImage() {
        navigator.camera.getPicture(uploadPhoto, function (message) {
            
        }, {
            quality: 100,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        });
    }

    function uploadPhoto(imageURI) {
        var params = {
            'Picture': imageURI
        };

        $('#imgUploading').show();
        appServices.CallService('userDetails', "POST", "api/Account/ChangeImage", params, function (res) {
            if (res != null) {
                language.openFrameworkModal('نجاح', 'تم رفع الصورة بنجاح .', 'alert', function () {
                    $('#imgUploading').hide();

                    CookieService.setCookie('usrPhoto', res);
                    var user = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    user.photoUrl = res;
                    CookieService.setCookie('userLoggedIn', JSON.stringify(user));
                    $scope.userDetailsForm.userImage = res;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                });
            }
            else {
                language.openFrameworkModal('خطأ', 'خطأ في إضافة صورة للمستخدم.', 'alert', function () { });
            }
        });
    }

    InitService.addEventListener('ready', function () {
        $scope.form = {};
        $scope.userDetailsForm = {};
        $scope.userDetailsForm.userImage = 'img/logo.png';

        app.onPageInit('userDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'userDetails') return;
            $rootScope.currentOpeningPage = 'userDetails';

        });

        app.onPageBeforeAnimation('userDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'userDetails') return;
            $rootScope.currentOpeningPage = 'userDetails';        
            $scope.resetForm();
            $scope.userDetailsReset = true;
            if (CookieService.getCookie('loginUsingSocial') == 'true') {
                $scope.editBySocial = true;
                $scope.emailRequired = false;
            }
            else {        
                $scope.emailRequired = true;
                $scope.editBySocial = false;

            }
            if (CookieService.getCookie('userLoggedIn') != null) {
                var userInformation = JSON.parse(CookieService.getCookie('userLoggedIn'));
                $scope.userDetailsForm.name = userInformation.name;
                if (typeof userInformation.age != 'undefined' && userInformation.age != null && parseInt(userInformation.age) > 0) {
                    $scope.userDetailsForm.age = userInformation.age;
                }
                $scope.userDetailsForm.address = userInformation.address;
                $scope.userDetailsForm.mobile = userInformation.phoneNumber;
                $scope.userDetailsForm.email = userInformation.email;

                if (typeof userInformation.photoUrl != 'undefined' && userInformation.photoUrl != null && userInformation.photoUrl != '' && userInformation.photoUrl != ' ') {
                    $scope.userDetailsForm.userImage = userInformation.photoUrl;
                }
                else {
                    $scope.userDetailsForm.userImage = 'img/logo.png';
                }
            }

            setTimeout(function () {
                $scope.$digest();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'userDetails') return;
            $rootScope.currentOpeningPage = 'userDetails';
            $('#imgUploading').hide();   
        });

        $scope.resetForm = function () {
            $scope.userDetailsReset = false;
            $scope.userDetailsForm.name = null;
            $scope.userDetailsForm.age = null;
            $scope.userDetailsForm.address = null;
            $scope.userDetailsForm.mobile = null;
            $scope.userDetailsForm.email = null;
            if (typeof $scope.editProfileForm != 'undefined' && $scope.editProfileForm != null) {
                $scope.editProfileForm.$setPristine(true);
                $scope.editProfileForm.$setUntouched();
            }
        };
        $scope.submitForm = function (isValid) {          
            if (isValid) {
                var name = $scope.userDetailsForm.name;
                var age = $scope.userDetailsForm.age;
                var address = $scope.userDetailsForm.address;
                var mobile = $scope.userDetailsForm.mobile;
                var email = $scope.userDetailsForm.email;

                var params = {
                    name: name,
                    age: age,
                    PhoneNumber: mobile,
                    email: email,
                    address: address
                };
        
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('userDetails', 'POST', "api/Account/ChangeInfo", params, function (response) {
                    SpinnerPlugin.activityStop();
                    if (response) {
                        $('#imgUploading').hide();
                        var userInformation = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        userInformation.name = $scope.userDetailsForm.name;
                        userInformation.age = $scope.userDetailsForm.age;
                        userInformation.address = $scope.userDetailsForm.address;
                        userInformation.phoneNumber = $scope.userDetailsForm.mobile;
                        userInformation.email = $scope.userDetailsForm.email;
                        CookieService.setCookie('userLoggedIn', JSON.stringify(userInformation));                      
                        helpers.GoBack();
                    }
                    else {
                        $('#imgUploading').hide();
                    }
                });
            }
        };
        
        $scope.AddImage = function () {
            getImage();
        };

        $scope.RemoveImage = function () {

            var params = {
                'Picture': 'none'
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('userDetails', "POST", "api/Account/ChangeImage", params, function (res) {
                SpinnerPlugin.activityStop();
                if (res != null) {
                    language.openFrameworkModal('نجاح', 'تم حذف الصورة بنجاح .', 'alert', function () {
                        $('#imgUploading').hide();

                        CookieService.setCookie('usrPhoto', '');
                        var user = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        user.photoUrl = '';
                        CookieService.setCookie('userLoggedIn', JSON.stringify(user));
                        $scope.userDetailsForm.userImage = 'img/logo.png';
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    });
                }
                else {
                    language.openFrameworkModal('خطأ', 'خطأ في حذف صورة للمستخدم.', 'alert', function () { });
                }
            });
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.ageRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

    });

}]);

