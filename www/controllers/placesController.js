﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('placesController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';
    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading = false;
    var categoryId;
    var subCategoryId;
    var subCategoryName;
    var selectedCategoryId
    var initPlacesInfinite;
    var allPlaces = [];

    InitService.addEventListener('ready', function () {
        $$('.placesFilter').hide();
        app.onPageInit('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';

            $$('#divInfinitePlaces').on('infinite', function () {
                $rootScope.load = true;
                    if (loading) return;
                    loading = true;
                    $scope.placesInfiniteLoader = true;

                    categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
                    var placesParamter = {
                        'categoryId': categoryId,
                        'subCategoryId': subCategoryId,
                        'PageNumber': parseInt(CookieService.getCookie('places-page-number')) + 1,
                        'PageSize': $rootScope.pageSize
                    }

                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    appServices.CallService('places', 'POST', "api/CategoryDetails/SearchCategoryResult",
                        placesParamter
                    , function (placesResult) {
                        SpinnerPlugin.activityStop();
                        if (placesResult && placesResult.length > 0) {
                            $scope.categoryName = placesResult[0].category;
                            loading = false;
                            CookieService.setCookie('places-page-number', parseInt(CookieService.getCookie('places-page-number')) + 1);
                            angular.forEach(placesResult, function (place) {
                                place.rating = parseInt(place.rating / 2);
                                place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                                allPlaces.push(place);
                            });
                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                            if (placesResult && placesResult.length <= $rootScope.pageSize) {
                                $scope.placesInfiniteLoader = false;
                                setTimeout(function () {
                                    $scope.$apply();
                                }, fw7.DelayBeforeScopeApply);
                                app.detachInfiniteScroll('#divInfinitePlaces');
                                return;
                            }
                        }
                        else {
                          $scope.placesInfiniteLoader = false;
                        }
                    });
            });

            $$('#divInfinitePlaces').on('ptr:refresh', function (e) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        if ($rootScope.swiper) {
                            $rootScope.swiper.destroy(true, true);
                        }
                        $rootScope.initalSwiper('#homeSwiper');
                        
                    }
                });
                $rootScope.load = true;
                updatePlaces();

            });
        });

        app.onPageBeforeAnimation('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $scope.txPlacesFilter = "";
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#Placeswiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');           
            $scope.selectedItem = $rootScope.CurrentuserCity;
            resetPlacesScope();
            $scope.places = "";
            $scope.cites = myApp.fw7.Cities;
          
        });

        app.onPageReinit('places', function (page) {
            if ($rootScope.swiper) {
                $rootScope.swiper.destroy(true, true);
            }
            $rootScope.initalSwiper('#Placeswiper');
            if ($rootScope.cityId) {
                $scope.selectedItem = $rootScope.cityId;
                categoryId = page.query.categoryId;
                selectedCategoryId = page.query.categoryId;
                subCategoryId = page.query.subCategoryId;
                subCategoryName = page.query.subCategoryName;
                //updatePlaces();
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
        });

        app.onPageAfterAnimation('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';
         
            categoryId = page.query.categoryId;
            selectedCategoryId = page.query.categoryId;
            subCategoryId = page.query.subCategoryId;
            subCategoryName = page.query.subCategoryName;
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            updatePlaces();
  
        });

        var updatePlaces = function () {
            resetPlacesScope();
            categoryId = typeof subCategoryId == 'undefined' || subCategoryId == null || subCategoryId == '' ? categoryId : '';
            subCategoryId = typeof subCategoryId != 'undefined' ? subCategoryId : '';
            var placesParamter = {
                'categoryId': categoryId,
                'subCategoryId': subCategoryId,
                'PageNumber': 1,
                'CityId': $rootScope.cityId.id,
                'PageSize': $rootScope.pageSize,
            }

            $$('.placesFilter').hide();

            appServices.CallService('places', 'POST', "api/CategoryDetails/SearchCategoryResult", placesParamter, function (placesResult) {
                    SpinnerPlugin.activityStop();
                    if (placesResult && placesResult.length > 0) {
                        $$('.placesFilter').show();
                        $scope.categoryName = placesResult[0].category;
                        $scope.placesInfiniteLoader = false;
                        app.attachInfiniteScroll('#divInfinitePlaces');
                        angular.forEach(placesResult, function (place) {
                            place.rating = parseInt(place.rating / 2);
                            place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                            allPlaces.push(place);
                        });
                        $scope.places = allPlaces;
                        app.pullToRefreshDone();
                    }
                    else {
                        $$('.placesFilter').hide();
                        $scope.placeAlert = true;
                        app.pullToRefreshDone();
                        $scope.placesInfiniteLoader = false;
                        $scope.categoryType = " لا يوجد " + subCategoryName;
                        if (selectedCategoryId == 1) {
                            $rootScope.tourismPlaces = true;
                            $scope.categoryName = "مطاعم";
                        }
                        if (selectedCategoryId == 2) {
                            //$scope.categoryType = "لا يوجد اماكن سياحية";
                            $scope.categoryName = "اماكن سياحية";
                        }
                        if (selectedCategoryId == 3) {
                            //$scope.categoryType = "لا يوجد اماكن تاريخية";
                            $scope.categoryName = "اماكن تاريخية";
                        }
                        if (selectedCategoryId == 4) {
                            //$scope.categoryType = "لا يوجد فنادق";
                            $scope.categoryName = " فنادق";
                        }
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
        }

        var resetPlacesScope = function () {
            CookieService.setCookie('places-page-number', 1);
            $scope.placeAlert = false;
            $scope.places = null;
            loading = false;
            allPlaces = [];
        }

        $scope.goToPlaceDetails = function (placeId) {
            app.pullToRefreshDone();
            helpers.GoToPage('placeDetails', { placeId: placeId });
        }

        $scope.GoToSearch = function () {
            app.pullToRefreshDone();
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
           
        };

        $scope.goToSearchResult = function (selectedCity) {
            app.pullToRefreshDone();
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            updatePlaces();
           
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        $scope.filterItems = function (filterText) {

            setTimeout(function () {

                if (filterText == '' || filterText == ' ') {
                    $scope.places = allPlaces;
                    $scope.categoryType = " لا يوجد " + subCategoryName;
                    $scope.placeAlert = $scope.places.length > 0 ? false : true;
                }
                else {
                    $scope.places = allPlaces.filter(function (item) {
                        return item.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    });
                    $scope.categoryType = " لا يوجد " + subCategoryName;
                    $scope.placeAlert = $scope.places.length > 0 ? false : true;
                }

                $scope.$apply();
            }, 100);
        };

        app.init();
    });

}]);

