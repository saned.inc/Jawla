﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.1.0.js" />

var serviceURL = 'http://jawlaapi.saned-projects.com/';
var hostUrl = 'http://jawlaapi.saned-projects.com/';
var lang = 'En';
var appToken = '';
var userId = 0;
userId = localStorage.getItem("UID");
var user;
var allCities = [];
var allCountries = [];
var allUsers = [];
var allAreas = [];
var BackIsClicked = true;
var scrollLoadsBefore = false;
var initLoginPage = true;
var initSignupPage = true;
var initactivationPage = true;
var initForgetPassword = true;
var initResetPassword = true;
var initChangePassword = true;
var initSearch = true;
var initSideMenu = true;
var clientId = 'consoleApp';
var clientSecret = '123@abc';
var initUserBlocked = false;
var  initDeviceback = true;
var messageInterval;
var myPhotoBrowserPopupDark;


myApp.angular.factory('InitService', ['$document', '$log', function ($document, $log) {
    'use strict';

    var pub = {},
      eventListeners = {
          'ready': []
      };

    pub.addEventListener = function (eventName, listener) {
        eventListeners[eventName].push(listener);
    };

    (function () {
        $document.ready(function () {
            var fw7 = myApp.fw7;
            var i;

            fw7.views.push(fw7.app.addView('.view-main', fw7.options));

            for (i = 0; i < eventListeners.ready.length; i = i + 1) {
                eventListeners.ready[i]();
            }
        });
       
        window.document.addEventListener('backbutton', function (event) {
            var fw7 = myApp.fw7;
                        
                var currentPage = fw7.views[0].activePage.name;
                if (currentPage == 'home' || currentPage == 'login') {
                   if (BackIsClicked) {
                        BackIsClicked = false;
                        ExitApplication();
                    }
                    else {
                        BackIsClicked = true;
                        fw7.app.closeModal('.modal-in');
                    }
                   
                }
                else {
                    fw7.app.closePanel();
                    if ($('.modal-in').length > 0) {
                        fw7.app.closeModal('.modal-in');
                        return false;
                    }
                    else {
                        if (currentPage == 'activation') {
                            return false;
                        }
                       
                        fw7.views[0].router.back();
                     
                    }
                }
         
        });

        window.document.addEventListener('pause', function (event) {

        });

        window.document.addEventListener('resume', function (event) {
            var fw7 = myApp.fw7;

            var currentPage = fw7.views[0].activePage.name;
        });

        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        window.addEventListener('native.keyboardshow', keyboardShowHandler);

        function ExitApplication() {
            if (navigator.app) {
                navigator.app.exitApp();
            }
            else if (navigator.device) {
                navigator.device.exitApp();
            }
        }

        document.addEventListener("offline", onOffline, false);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        window.addEventListener('native.keyboardshow', keyboardShowHandler);

        function onOffline() {
            navigator.notification.alert('انت غير متصل بالانترنت , من فضلك أعد تشغيل البرنامج بعد التأكد من الإنترنت .', function () {
                ExitApplication();
            }, 'خطأ', 'موافق');
        }

        function keyboardHideHandler(e) {
            if ($('.ui-footer').hasClass('ui-fixed-hidden')) {
                $('.ui-footer').removeClass('ui-fixed-hidden');
            }
        }

        function keyboardShowHandler(e) {
            $('.ui-footer').addClass('ui-fixed-hidden');
        }


    }());

    return pub;

}]);
